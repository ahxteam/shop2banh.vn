﻿var commentController = {
    currentComment: 0,
    deleteComment: function () {
        $.ajax({
            method: "POST",
            url: "/admin/comment/delete",
            data: {
                id: commentController.currentComment
            },
            async: false
        }).done(function (response) {
            window.location.reload();
        });
    }
};