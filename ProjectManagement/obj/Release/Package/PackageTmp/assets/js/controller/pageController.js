﻿var pageController = {
    currentPage: 0,
    showFormAddNew: function () {
        $.ajax({
            method: "GET",
            url: "/admin/pages/createoredit",
            async: false,
            data: {
                id: pageController.currentPage
            },
        }).done(function (response) {
            debugger;
            $('.modal').remove();
            $("body").append(response);
            try {
                tinymce.remove();
                tinymce.init({
                    selector: "textarea#Content",
                    theme: "modern",
                    height: 250,
                    menubar: false,
                    statusbar: false,
                    image_caption: true,
                    plugins: [
                        "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
                        "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                        "save table contextmenu directionality emoticons template paste textcolor"
                    ],
                    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons",
                    style_formats: [
                        { title: 'Bold text', inline: 'b' },
                        { title: 'Red text', inline: 'span', styles: { color: '#ff0000' } },
                        { title: 'Red header', block: 'h1', styles: { color: '#ff0000' } },
                        { title: 'Example 1', inline: 'span', classes: 'example1' },
                        { title: 'Example 2', inline: 'span', classes: 'example2' },
                        { title: 'Table styles' },
                        { title: 'Table row 1', selector: 'tr', classes: 'tablerow1' }
                    ],
                    file_browser_callback: function (field_name, url, type, win) {
                        if (type == 'image') $('#image_form input').click();
                    }
                });
            } catch (e) {

            }
            $("button[name=add-new]").click();
        });
    },
    deletePage: function () {
        $.ajax({
            method: "POST",
            url: "/admin/pages/delete",
            data: {
                id: pageController.currentPage
            },
            async: false
        }).done(function (response) {
            window.location.reload();
        });
    },
    saveData: function () {
        var model = {
            Id: $("#Id").val(),
            Name: $("#Name").val(),
            Description: $("#Description").val(),
            UrlCode: $("#UrlCode").val(),
            Active: $("input[name=Active]:checked").val()==true ? true : false,
            OrderDisplay: $("#OrderDisplay").val(),
            Content: tinymce.get('Content').getContent()
        }
        var vld = window.baseJs.validation;
        var $form = $("#createoredit");
        var jsonData = JSON.stringify(model);
        $.ajax({
            url: '/admin/pages/createoredit',
            data: jsonData,
            type: 'POST',
            contentType: 'application/json',
            dataType: 'json',
            cache: false,
            success: function (response) {
                if (response.result === true) {
                    $('#con-close-modal').modal('toggle');
                    window.location.reload();
                } else if (response.Errors) {
                    vld.clear($form);
                    vld.all(response.Errors, $form);
                } else {
                    $('#con-close-modal').modal('toggle');
                }
            }
        });
    },
};