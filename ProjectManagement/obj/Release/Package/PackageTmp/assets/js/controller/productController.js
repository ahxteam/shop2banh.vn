﻿var productController = {
    currentProduct:0,
    deleteProduct: function () {
        $.ajax({
            method: "POST",
            url: "/admin/product/delete",
            data: {
                id: productController.currentProduct
            },
            async: false
        }).done(function (response) {
            window.location.reload();
        });
    }
};