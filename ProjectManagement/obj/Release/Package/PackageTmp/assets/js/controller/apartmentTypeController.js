﻿var apartmentTypeController = {
    currentApartmentType: 0,
    showFormAddNew: function () {
        $.ajax({
            method: "GET",
            url: "/admin/apartmentType/createoredit",
            async: false,
            data: {
                id: apartmentTypeController.currentApartmentType
            },
        }).done(function (response) {
            $('.modal').remove();
            $("body").append(response);
            $("button[name=add-new]").click();
        });
    },
    deleteApartmentType: function () {
        $.ajax({
            method: "POST",
            url: "/admin/apartmentType/delete",
            data: {
                id: apartmentTypeController.currentApartmentType
            },
            async: false
        }).done(function (response) {
            window.location.reload();
        });
    },
    saveData: function () {
        var model = {
            Id: $("#Id").val(),
            Name: $("#Name").val(),
            TypeName: $("#TypeName").val(),
            Acreage: $("#Acreage").val(),
            Parent: $("#Parent").val(),
            Active: $("input[name=Active]:checked").val()=="true" ? true : false,
            DisplayOrder: $("#OrderDisplay").val()
        }
        var vld = window.baseJs.validation;
        var $form = $("#createoredit");
        var jsonData = JSON.stringify(model);
        $.ajax({
            url: '/admin/menu/createoredit',
            data: jsonData,
            type: 'POST',
            contentType: 'application/json',
            dataType: 'json',
            cache: false,
            success: function (response) {
                if (response.result === true) {
                    $('#con-close-modal').modal('toggle');
                    window.location.reload();
                } else if (response.Errors) {
                    vld.clear($form);
                    vld.all(response.Errors, $form);
                } else {
                    $('#con-close-modal').modal('toggle');
                }
            }
        });
    },
};