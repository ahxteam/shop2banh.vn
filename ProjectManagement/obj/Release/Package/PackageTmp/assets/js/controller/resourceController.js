﻿var resourceController = {
    currentResource:0,
    showFormAddNew: function () {
        $.ajax({
            method: "GET",
            url: "/admin/resource/createoredit",
            async: false,
            data: {
                id: resourceController.currentResource
            },
        }).done(function (response) {
            $('.modal').remove();
            $("body").append(response);
            $("button[name=add-new]").click();
        });
    },
    deleteResource: function () {
        $.ajax({
            method: "POST",
            url: "/admin/resource/delete",
            data: {
                id: resourceController.currentResource
            },
            async: false
        }).done(function (response) {
            window.location.reload();
        });
    },
    saveData: function () {
        var model = {
            Id: $("#Id").val(),
            Name: $("#Name").val(),
            Key: $("#Key").val(),
            Value: $("#Value").val()
        }
        var vld = window.baseJs.validation;
        var $form = $("#createoredit");
        var jsonData = JSON.stringify(model);
        $.ajax({
            url: '/admin/resource/createoredit',
            data: jsonData,
            type: 'POST',
            contentType: 'application/json',
            dataType: 'json',
            cache: false,
            success: function (response) {
                if (response.result === true) {
                    $('#con-close-modal').modal('toggle');
                    window.location.reload();
                } else if (response.Errors) {
                    vld.clear($form);
                    vld.all(response.Errors, $form);
                } else {
                    $('#con-close-modal').modal('toggle');
                }
            }
        });
    },
};