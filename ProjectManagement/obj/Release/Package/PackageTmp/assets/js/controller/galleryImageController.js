﻿var galleryImageController = {
    currentGalleryImage: 0,
    deleteImage: function () {
        $.ajax({
            method: "POST",
            url: "/admin/gallery/deleteimage",
            data: {
                id: galleryImageController.currentGalleryImage
            },
            async: false
        }).done(function (response) {
            window.location.reload();
        });
    }
};