﻿var realStructController = {
    currentRealStruct: 0,
    deleteRealStruct: function () {
        $.ajax({
            method: "POST",
            url: "/admin/realstructadmin/delete",
            data: {
                id: realStructController.currentRealStruct
            },
            async: false
        }).done(function (response) {
            window.location.reload();
        });
    },
};