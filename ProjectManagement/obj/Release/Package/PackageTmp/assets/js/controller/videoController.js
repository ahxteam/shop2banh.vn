﻿var videoController = {
    currentVideo: 0,
    deleteVideo: function () {
        $.ajax({
            method: "POST",
            url: "/admin/video/delete",
            data: {
                id: videoController.currentVideo
            },
            async: false
        }).done(function (response) {
            window.location.reload();
        });
    },
};