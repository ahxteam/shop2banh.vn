﻿
function showform_post(type) {
    if (type == 1) {
        jQuery('#cus_postdanhgia_mau').show();
    } else {
        jQuery('#cus_postdanhgia_mau').hide();
    }

}
function calculatePoint() {
    // Tinh tong diem danh gia
    var point_sum = 0, point_count = 0, point_part = 0, point_avg = 0;
    var point_part1 = parseInt($('#rate-point1').val());
    var point_part2 = parseInt($('#rate-point2').val());
    var point_part4 = parseInt($('#rate-point4').val());
    var point_part5 = parseInt($('#rate-point5').val());
    var point_part6 = parseInt($('#rate-point6').val());
    if (isNaN(point_part1))
        point_part1 = 0;
    point_sum += point_part1;
    point_count++;
    if (isNaN(point_part2))
        point_part2 = 0;
    point_sum += point_part2;
    point_count++;
    if (isNaN(point_part4))
        point_part4 = 0;
    point_sum += point_part4;
    point_count++;
    if (isNaN(point_part5))
        point_part5 = 0;
    point_sum += point_part5;
    point_count++;
    if (isNaN(point_part6))
        point_part6 = 0;
    point_sum += point_part6;
    point_count++;
    //                var arrId = [2, 1, 4, 5, 6];
    //        for (var i in arrId) {
    //            point_part = parseInt($('#rate-point' + arrId[i]).val());
    //            if (isNaN(point_part))
    //                point_part = 0;
    //            point_sum += point_part;
    //            point_count++;
    //        }
    point_avg = Math.round(point_sum / point_count * 10) / 10;
    $('.v2_project_dgn_spr_point').html(new String(point_avg) + '/5');
    $('#rate-point').rating('update', point_avg);
}
function save_comment(type, ele) {
    if (type == 1) {
        var id = ele.id;
        var commentId = id.replace(/(traloi-)|(-btn)/g, '');
        var content = document.getElementById('traloi-' + commentId + '-input').value;

        if (jQuery.trim(content) == '') {
            displayModel('Vui lòng nhập nội dung trả lời');
            jQuery('#traloi-' + commentId + '-input').focus();
            return;
        }
        jQuery('#btn_send_bl').addClass('hidden');
        jQuery('#btn_send_tl').removeClass('hidden');
        jQuery('#com_par').val(commentId);
    } else {
        var content = document.getElementById('rate-content').value;

        var point = parseFloat(document.getElementById('rate-point').value);
        var content = document.getElementById('rate-content').value;

        var point_price = parseInt(document.getElementById('rate-point1').value);
        var point_location = parseInt(document.getElementById('rate-point2').value);
        var point_service = parseInt(document.getElementById('rate-point4').value);
        var point_design = parseInt(document.getElementById('rate-point5').value);
        var point_trust = parseInt(document.getElementById('rate-point6').value);
        if (isNaN(point_price) || point_price == 0
                || isNaN(point_location) || point_location == 0
                || isNaN(point_service) || point_service == 0
                || isNaN(point_design) || point_design == 0
                || isNaN(point_trust) || point_trust == 0) {
            displayModel('Vui lòng di chuột lên sao để chọn điểm đánh giá');
            return;
        }

        if (jQuery.trim(content) == '') {
            displayModel('Vui lòng nhập nội dung đánh giá');
            jQuery('#rate-content').focus();
            return;
        }
        jQuery('#btn_send_bl').removeClass('hidden');
        jQuery('#btn_send_tl').addClass('hidden');
    }
    $("#com_captcha-image").trigger("click");
    $("#com_captcha").val("");
    $('#modal_save_temp').modal('show');
}
function displayModel(message) {
    $("#content-alert").html(message);
    $('#message-notification').modal('show');
}
function fLike(id) {
    var csrfParam = $('#form-key-csrf').attr('data-key-name');
    var csrfToken = $('#form-key-csrf').attr('data-key-value');
    //alert('Like comment co id =' + id);
    var dataPost = { id: id };
    var key_check_user_like = 'user_like' + id;
    var val_cookies_like = Cookies.get(key_check_user_like);
    if (val_cookies_like == 1) {
        alert("Bạn đã thích đánh giá này trước đó rồi.");
    } else {

        jQuery.ajax({
            url: '/comment/like.html',
            type: "POST",
            data: dataPost,
            dataType: "json",
            success: function (obj) {
                if (obj.err === 0) {
                    Cookies.set(key_check_user_like, '1', {
                        expires: 365,
                        path: "/"
                    });
                    var nbLike = parseInt(jQuery('#nb-like-' + id).text());
                    jQuery('#nb-like-' + id).html(nbLike + 1);
                }
            }
        });
    }

}

function fReply(id) {
    var idDiv = 'traloi-' + id + '-wrapper';
    var div = document.getElementById(idDiv);
    if (div.style.display == 'none') {
        div.style.display = 'block';
    } else {
        div.style.display = 'none';
    }
}

function fPostReply(ele) {
    var csrfParam = $('#form-key-csrf').attr('data-key-name');
    var csrfToken = $('#form-key-csrf').attr('data-key-value');
    var id = ele.id;
    var commentId = id.replace(/(traloi-)|(-btn)/g, '');
    var content = document.getElementById('traloi-' + commentId + '-input').value;

    if (jQuery.trim(content) == '') {
        alert('Vui lòng nhập nội dung trả lời');
        jQuery('#traloi-' + commentId + '-input').focus();
        return;
    }
    var dataPost = { content: content, id: commentId };
    dataPost[csrfParam] = csrfToken;
    jQuery.ajax({
        url: '/comment/replynouser.html',
        type: "POST",
        data: dataPost,
        dataType: "json",
        success: function (obj) {
            if (obj.err === 0) {
                trackingchat(9, id);
                var nbReply = parseInt(jQuery('#nb-reply-' + commentId).text());
                jQuery('#nb-reply-' + commentId).html(nbReply + 1);

                var divSubmit = '<div class="v2_danhgia_ns_bcc_bi_item_child">' +
                        '<div class="v2_danhgia_ns_bcc_bi_ava">' +
                        '<img src="ver2/images/dev/chitiet/logocus.jpg" width="70" height="70" alt="">' +
                        '</div>' +
                        '<div class="v2_danhgia_ns_bcc_bi_info">' +
                        '<div class="v2_danhgia_ns_bcc_bi_if_name">' +
                        '<div class="v2_danhgia_ns_bcc_bi_if_name_text">' + ((obj.name != '') ? obj.name : obj.email) + '</div>' +
                        '<div class="c"></div>' +
                        '</div>' +
                        '<div class="v2_danhgia_ns_bcc_bi_if_text">' +
                        content.replace(/</g, '&lt;') +
                        '</div>' +
                        '</div>' +
                        '<div class="c"></div>' +
                        '</div>';
                jQuery('#traloi-' + commentId + '-list').append(divSubmit);

                // Reset o nhap noi dung
                jQuery('#traloi-' + commentId + '-input').val('');
                jQuery('#rate-point').rating('clear');
            } else {
                alert('Có lỗi xảy ra trong quá trình trả lời. Vui lòng thử lại sau!');
            }
        }
    });
}

function fPostRate() {
    var csrfParam = $('#form-key-csrf').attr('data-key-name');
    var csrfToken = $('#form-key-csrf').attr('data-key-value');
    var point = parseFloat(document.getElementById('rate-point').value);
    var content = document.getElementById('rate-content').value;

    var point_price = parseInt(document.getElementById('rate-point1').value);
    var point_location = parseInt(document.getElementById('rate-point2').value);
    var point_service = parseInt(document.getElementById('rate-point4').value);
    var point_design = parseInt(document.getElementById('rate-point5').value);
    var point_trust = parseInt(document.getElementById('rate-point6').value);
    if (isNaN(point_price) || point_price == 0
            || isNaN(point_location) || point_location == 0
            || isNaN(point_service) || point_service == 0
            || isNaN(point_design) || point_design == 0
            || isNaN(point_trust) || point_trust == 0) {
        alert('Vui lòng di chuột lên sao để chọn điểm đánh giá');
        return;
    }

    if (jQuery.trim(content) == '') {
        alert('Vui lòng nhập nội dung đánh giá');
        jQuery('#rate-content').focus();
        return;
    }


    var dataPost = { point: point, content: content, id: id };
    dataPost['point_price'] = $('#rate-point1').val();
    dataPost['point_location'] = $('#rate-point2').val();
    dataPost['point_service'] = $('#rate-point4').val();
    dataPost['point_design'] = $('#rate-point5').val();
    dataPost['point_trust'] = $('#rate-point6').val();
    dataPost[csrfParam] = csrfToken;
    jQuery.ajax({
        url: '/comment/ratenouser.html',
        type: "POST",
        data: dataPost,
        dataType: "json",
        success: function (obj) {
            if (obj.err === 0) {
                trackingchat(9, id);

                jQuery('#rate-wrapper').prepend(obj.html);
                jQuery('.rating').rating('refresh');

                // Reset o nhap
                jQuery('#rate-content').val('');
                showform_post(2);
            } else if (obj.err === 1) {
                loginModal();
            } else {
                alert('Có lỗi xảy ra trong quá trình đánh giá. Vui lòng thử lại sau!');
            }
        }
    });
}

function loadMore() {
    var pageSize = jQuery("#page-size").val();
   
    jQuery.ajax({
        method: "GET",
        url: '/ratingview/GetCommentListAjax',
        async: false,
        data: {
            pageSize: pageSize
        },
    }).done(function (response) {
        jQuery('#rate-wrapper').append(response);
        jQuery("#page-size").val(jQuery("#page-size").val() + 1);
        jQuery('.rating').rating('refresh');
        if (response.trim().length < 1) {
            jQuery('#btn-loadmore').hide();
        }
    });
}

function calculatePoint() {
    // Tinh tong diem danh gia
    var point_sum = 0, point_count = 0, point_part = 0, point_avg = 0;
    var point_part1 = parseInt($('#rate-point1').val());
    var point_part2 = parseInt($('#rate-point2').val());
    var point_part4 = parseInt($('#rate-point4').val());
    var point_part5 = parseInt($('#rate-point5').val());
    var point_part6 = parseInt($('#rate-point6').val());
    if (isNaN(point_part1))
        point_part1 = 0;
    point_sum += point_part1;
    point_count++;
    if (isNaN(point_part2))
        point_part2 = 0;
    point_sum += point_part2;
    point_count++;
    if (isNaN(point_part4))
        point_part4 = 0;
    point_sum += point_part4;
    point_count++;
    if (isNaN(point_part5))
        point_part5 = 0;
    point_sum += point_part5;
    point_count++;
    if (isNaN(point_part6))
        point_part6 = 0;
    point_sum += point_part6;
    point_count++;
    //                var arrId = [2, 1, 4, 5, 6];
    //        for (var i in arrId) {
    //            point_part = parseInt($('#rate-point' + arrId[i]).val());
    //            if (isNaN(point_part))
    //                point_part = 0;
    //            point_sum += point_part;
    //            point_count++;
    //        }
    point_avg = Math.round(point_sum / point_count * 10) / 10;
    $('.v2_project_dgn_spr_point').html(new String(point_avg) + '/5');
    $('#rate-point').rating('update', point_avg);
}
function validateEmail(email) {
    var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if (!filter.test(email)) {
        return false;
    }
    return true;
}
function rateparent() {

    var content = document.getElementById('rate-content').value;
    var name = jQuery('#com_name').val();
    var email = jQuery('#com_email').val();
    var sdt = jQuery('#com_phone').val();
    var com_captcha = jQuery('#com_captcha').val();

    var point = parseFloat(document.getElementById('rate-point').value);
    var content = document.getElementById('rate-content').value;

    var point_price = parseInt(document.getElementById('rate-point1').value);
    var point_location = parseInt(document.getElementById('rate-point2').value);
    var point_service = parseInt(document.getElementById('rate-point4').value);
    var point_design = parseInt(document.getElementById('rate-point5').value);
    var point_trust = parseInt(document.getElementById('rate-point6').value);
    if (isNaN(point_price) || point_price == 0
            || isNaN(point_location) || point_location == 0
            || isNaN(point_service) || point_service == 0
            || isNaN(point_design) || point_design == 0
            || isNaN(point_trust) || point_trust == 0) {
        displayModel('Vui lòng di chuột lên sao để chọn điểm đánh giá');
        return;
    }

    if (jQuery.trim(content) == '') {
        displayModel('Vui lòng nhập nội dung đánh giá');
        jQuery('#rate-content').focus();
        return;
    }

    //Validate
    if (name == '') {
        displayModel('Bạn chưa nhập họ tên.');
        jQuery('#com_name').focus();
        return;
    }
    if (sdt != '') {
        if (!validatePhone(sdt)) {
            displayModel('Số điện thoại bạn nhập sai định dạng.');
            jQuery('#com_phone').focus();
            return;
        }
    }
    if (email == '') {
        displayModel('Bạn chưa nhập email.');
        jQuery('#com_email').focus();
        return;
    }
    if (!validateEmail(email)) {
        displayModel('Email bạn nhập sai định dạng.');
        jQuery('#com_email').focus();
        return;
    }
    if (com_captcha == '') {
        displayModel('Bạn chưa nhập captcha.');
        jQuery('#com_captcha').focus();
        return;
    }
    if (validateCaptcha() == false) {
        displayModel('Captcha không chính xác.');
        jQuery('#com_captcha').focus();
        return;
    }
    var dataPost = {
        Name: name,
        Email: email,
        Phone: sdt,
        Captcha: com_captcha,
        PriceRating: $('#rate-point1').val(),
        PositionRating: $('#rate-point2').val(),
        ServiceRating: $('#rate-point4').val(),
        DesignRating: $('#rate-point5').val(),
        InvestorRating: $('#rate-point6').val(),
        Comment: content
    };


    jQuery.ajax({
        url: '/ratingview/rate',
        type: "POST",
        data: dataPost,
        dataType: "json",
        success: function (obj) {
            if (obj.status == true) {
                jQuery('#rate-wrapper').prepend(obj.html);
                jQuery('.rating').rating('refresh');
                // Reset o nhap
                jQuery('#rate-content').val('');
                showform_post(2);
                jQuery('#modal_save_temp').modal('hide');
                displayModel('Cảm ơn ý kiến đánh giá của bạn!');
            } else {
                displayModel('Có lỗi xảy ra trong quá trình đánh giá. Vui lòng thử lại sau!');
            }
        }
    });
}
function rateReply() {
   
    var commentId = document.getElementById('com_par').value;
    var content = document.getElementById('traloi-' + commentId + '-input').value;
    var com_captcha = jQuery('#com_captcha').val();

    if (jQuery.trim(content) == '') {
        alert('Vui lòng nhập nội dung trả lời');
        jQuery('#traloi-' + commentId + '-input').focus();
        return;
    }
    var name = jQuery('#com_name').val();
    var email = jQuery('#com_email').val();
    var sdt = jQuery('#com_phone').val();

    //Validate
    if (name == '') {
        alert('Bạn chưa nhập họ tên.');
        jQuery('#com_name').focus();
        return;
    }
    if (sdt != '') {
        if (!validatePhone(sdt)) {
            alert('Số điện thoại bạn nhập sai định dạng.');
            jQuery('#com_phone').focus();
            return;
        }
    }

    if (email == '') {
        alert('Bạn chưa nhập email.');
        jQuery('#com_email').focus();
        return;
    }
    if (!validateEmail(email)) {
        alert('Email bạn nhập sai định dạng.');
        jQuery('#com_email').focus();
        return;
    }
    if (com_captcha == '') {
        alert('Bạn chưa nhập captcha.');
        jQuery('#com_captcha').focus();
        return;
    }

    var dataPost = {
        Name: name,
        Email: email,
        Phone: sdt,
        Captcha: com_captcha,
        PriceRating: $('#rate-point1').val(),
        PositionRating: $('#rate-point2').val(),
        ServiceRating: $('#rate-point4').val(),
        DesignRating: $('#rate-point5').val(),
        InvestorRating: $('#rate-point6').val(),
        Comment: content,
        RatingParent : commentId
    };


    jQuery.ajax({
        url: '/ratingview/rate',
        type: "POST",
        data: dataPost,
        dataType: "json",
        success: function (obj) {
            if (obj.status == true) {
                jQuery('#rate-wrapper').prepend(obj.html);
                jQuery('.rating').rating('refresh');
                // Reset o nhap
                jQuery('#rate-content').val('');
                showform_post(2);
                jQuery('#modal_save_temp').modal('hide');
                displayModel('Cảm ơn ý kiến đánh giá của bạn!');
            } else {
                displayModel('Có lỗi xảy ra trong quá trình đánh giá. Vui lòng thử lại sau!');
            }
        }
    });
}
function goComment(id_reply, page, content) {
    if (page > 0) {
        window['_PAGE_COMMENT'] = 1;
        for (i = 0; i < page - 1; i++) {
            loadMore();
        }
    }
    setTimeout(function () {
        if ($('#nb-reply-' + id_reply).length > 0) {
            fReply(id_reply);
            jQuery('#traloi-' + id_reply + '-input').val(content);
            if ($('.detail_v2').length > 0) {
                $("#danhgia_live .nano").nanoScroller({ scrollTop: $('#traloi-' + id_reply + '-input').offset().top - 130 });
            } else {
                $('html, body').stop().animate({
                    scrollTop: $('#traloi-' + id_reply + '-input').offset().top - 130
                }, 400);
            }
        }
    }, 1000);
}




//===============================================================================================================

function tltt_step4(pclass, vclass, type) {
    err_tltt(pclass, '');
    var csrfParam = $('#form-key-csrf').attr('data-key-name');
    var csrfToken = $('#form-key-csrf').attr('data-key-value');
    var project = '467';
    var email = $.trim($('.' + pclass + ' #sub_email').val());
    if (!checkmail_dl(email)) {
        tltt_showaction(pclass, '2');
        $('.' + pclass + ' #sub_email').val('');
        $('.' + pclass + ' #sub_email').focus();
        return false;
    }
    var phone = $.trim($('.' + pclass + ' #sub_phone').val());
    if (!phone) {
        err_tltt(pclass, 'Vui lòng nhập số điện thoại');
        $('.' + pclass + ' #sub_phone').focus();
        tltt_showaction(pclass, '3');
        return false;
    }
    if (!validatePhone(phone)) {
        err_tltt(pclass, 'Vui lòng nhập số điện thoại hợp lệ');
        $('.' + pclass + ' #sub_phone').focus();
        tltt_showaction(pclass, '3');
        return false;
    }
    var key = $.trim($('.' + pclass + ' #sub_code').val());
    if (key == '') {
        err_tltt(pclass, 'Vui lòng nhập mã xác nhận');
        $('.' + pclass + ' #sub_code').focus();
        return false;
    }
    var youwant = '';
    $('.' + pclass + ' input.' + vclass + ':checked').each(function () {
        youwant += $(this).val() + ',';
    });
    var dataPost = { 'email': email, 'project': project, 'type_key': type, 'type': 5, 'subtype': 1, 'object_type': 2, 'phone': phone, 'key': key, 'data_sendmail': youwant, 'position': 'fptcity-right' };
    dataPost[csrfParam] = csrfToken;
    jQuery.ajax({
        url: '/price/sendmailtkg.html',
        type: "POST",
        data: dataPost,
        dataType: "json",
        beforeSend: function () {
            $("#ajax-loading").fadeIn();
            is_ajax_loading = true;
        }, complete: function () {
            $('#ajax-loading').fadeOut();
        },
        success: function (data) {
            if (data.action) {
                if (data.err === 0) {
                    trackingchat(12, '467');
                    chatsubscribe("Tải bộ tài liệu mới nhất");
                    tltt_showaction(pclass, '5');
                } else {
                    alert('Có lỗi xảy ra. Vui lòng thử lại sau!');
                }
            } else {
                if (data.err == '1') {
                    alert('Có lỗi xảy ra. Vui lòng thử lại sau!');
                    location.reload();
                    return false;
                } else if (data.err == 'k1') {
                    err_tltt(pclass, 'Vui lòng nhập mã xác nhận');
                    $('.' + pclass + ' #sub_code').focus();
                    return false;
                } else if (data.err == 'k2') {
                    err_tltt(pclass, 'Mã xác nhận không đúng.Vui lòng kiểm tra lại');
                    $('.' + pclass + ' #sub_code').focus();
                    return false;
                }
            }
        }
    });

}
function re_key_downtltt(pclass, type) {
    err_tltt(pclass, '');
    var email = $.trim($('.' + pclass + ' #sub_email').val());
    var phone = $.trim($('.' + pclass + ' #sub_phone').val());
    var dataPost = { 'email': email, 'phone': phone, 'type': type };
    jQuery.ajax({
        url: '/deal/resendkeyphonedownload.html',
        type: "POST",
        data: dataPost,
        dataType: "json",
        async: false,
        beforeSend: function () {
        },
        success: function (data) {
            if (data.action) {
                $('.' + pclass + ' #sub_code').val('');
                $('.' + pclass + ' #sub_code').focus();
                $('.' + pclass + ' .text_phone').html(phone);
                $('.' + pclass + ' .text_seconds').html('90s').attr('data-count', 90);
                $('.' + pclass + ' .text_seconds').removeClass('hidden');
                $('.' + pclass + ' .text_resend_ma').addClass('hidden');
                IntervalTime_pro = setInterval(function () {
                    updateTime_tltt(pclass)
                }, 1000);
            } else {
                if (data.err == '1') {
                    alert('Có lỗi khi gửi lại mã xác nhận');
                    location.reload();
                    return false;
                } else if (data.err == 'me1') {
                    alert('Có lỗi khi gửi tin nhắn tới số điện thoại');
                    $('.' + pclass + ' #sub_phone').focus();
                    return false;
                }
            }
        }
    });
}
function tltt_step3(pclass, type) {
    err_tltt(pclass, '');
    var email = $.trim($('.' + pclass + ' #sub_email').val());
    if (!checkmail_dl(email)) {
        tltt_showaction(pclass, '2');
        $('.' + pclass + ' #sub_email').val('');
        $('.' + pclass + ' #sub_email').focus();
        return false;
    }
    var phone = $.trim($('.' + pclass + ' #sub_phone').val());
    if (!phone) {
        err_tltt(pclass, 'Vui lòng nhập số điện thoại');
        $('.' + pclass + ' #sub_phone').focus();
        return false;
    }
    if (!validatePhone(phone)) {
        err_tltt(pclass, 'Vui lòng nhập số điện thoại hợp lệ');
        $('.' + pclass + ' #sub_phone').focus();
        return false;
    }
    var check_dowload = verifyuser_dowload(email, phone);
    if (check_dowload == 2) {
        $('.' + pclass + ' #sub_code').val('904135');
        tltt_step4(pclass, 'chktailieu', 'tailieu');
    } else {
        var dataPost = { 'email': email, 'phone': phone, 'type': type };
        jQuery.ajax({
            url: '/deal/sendkeyphonedownload.html',
            type: "POST",
            data: dataPost,
            dataType: "json",
            async: false,
            beforeSend: function () {
            },
            success: function (data) {
                if (data.action) {
                    trackingchat(11, '467', '', phone);
                    tltt_showaction(pclass, '4');
                    $('.' + pclass + ' #sub_code').val('');
                    $('.' + pclass + ' #sub_code').focus();
                    $('.' + pclass + ' .text_phone').html(phone);
                    $('.' + pclass + ' .text_seconds').html('90s').attr('data-count', 90);
                    $('.' + pclass + ' .text_seconds').removeClass('hidden');
                    $('.' + pclass + ' .text_resend_ma').addClass('hidden');
                    IntervalTime_pro = setInterval(function () {
                        updateTime_tltt(pclass)
                    }, 1000);
                } else {
                    if (data.err == 'e1') {
                        err_tltt(pclass, 'Vui lòng nhập email');
                        tltt_showaction(pclass, '2');
                        $('.' + pclass + ' #sub_email').val('');
                        $('.' + pclass + ' #sub_email').focus();
                        return false;
                    } else if (data.err == 'e2') {
                        err_tltt(pclass, 'Vui lòng nhập email hợp lệ');
                        tltt_showaction(pclass, '2');
                        $('.' + pclass + ' #sub_email').val('');
                        $('.' + pclass + ' #sub_email').focus();
                        return false;
                    } else if (data.err == 'p1') {
                        err_tltt(pclass, 'Vui lòng nhập số điện thoại');
                        $('.' + pclass + ' #sub_phone').val('');
                        $('.' + pclass + ' #sub_phone').focus();
                        return false;
                    } else if (data.err == 'p2') {
                        err_tltt(pclass, 'Vui lòng nhập số điện thoại hợp lệ');
                        $('.' + pclass + ' #sub_phone').val('');
                        $('.' + pclass + ' #sub_phone').focus();
                        return false;
                    } else if (data.err == 'me1') {
                        err_tltt(pclass, 'Có lỗi khi gửi tin nhắn tới số điện thoại');
                        $('.' + pclass + ' #sub_phone').focus();
                        return false;
                    }
                }
            }
        });
    }
}
function updateTime_tltt(pclass) {
    var data_count = $.trim($('.' + pclass + ' .text_seconds').attr('data-count'));
    data_count = parseInt(data_count) - 1;
    if (parseInt(data_count)) {
        $('.' + pclass + ' .text_seconds').html(parseInt(data_count) + 's').attr('data-count', parseInt(data_count));
    } else {
        clearInterval(IntervalTime_pro);
        $('.' + pclass + ' .text_seconds').addClass('hidden');
        $('.' + pclass + ' .text_resend_ma').removeClass('hidden');
    }
}
function tltt_step2(pclass) {
    var check_dowload = checkuser_dowload();
    err_tltt(pclass, '');
    var email = $.trim($('.' + pclass + ' #sub_email').val());
    if (!checkmail_dl(email, pclass)) {
        return false;
    }
    trackingchat(10, '467', email);
    if (check_dowload == 2) {
        //Tải luon
        $('.' + pclass + ' #sub_code').val('904135');
        tltt_step4(pclass, 'chktailieu', 'tailieu');
    } else {
        tltt_showaction(pclass, '3');
        $('.' + pclass + ' #sub_phone').focus();
    }
}
function tltt_step1(pclass, vclass) {
    err_tltt(pclass, '');
    var checked_checkboxes = jQuery('.' + pclass + " input." + vclass + ":checked");
    if (!checked_checkboxes.length) {
        err_tltt(pclass, 'Bạn chưa chọn thông tin dự án muốn nhận');
        return false;
    }
    var check_dowload = checkuser_dowload('sub_email', 'sub_phone');
    if (check_dowload == 1) {
        //Tải luon
        $('.' + pclass + ' #sub_code').val('904135');
        tltt_step4(pclass, vclass, 'tailieu');
    } else {
        tltt_showaction(pclass, '2');
        //$('.' + pclass + ' #sub_email').val('');
        $('.' + pclass + ' #sub_email').focus();
    }

}
function checkmail_dl(email, pclass) {
    if (!email) {
        err_tltt(pclass, 'Vui lòng nhập email');
        $('.' + pclass + ' #sub_email').focus();
        return false;
    } else if (!validateEmail(email)) {
        err_tltt(pclass, 'Vui lòng nhập email hợp lệ');
        $('.' + pclass + ' #sub_email').focus();
        return false;
    } else {
        return true;
    }
}
function tltt_back(pclass, stt) {
    err_tltt(pclass, '');
    tltt_showaction(pclass, stt);
}
function tltt_showaction(pclass, stt) {
    jQuery('.' + pclass + ' .step1').addClass('hidden');
    jQuery('.' + pclass + ' .step2').addClass('hidden');
    jQuery('.' + pclass + ' .step3').addClass('hidden');
    jQuery('.' + pclass + ' .step4').addClass('hidden');
    jQuery('.' + pclass + ' .step5').addClass('hidden');
    jQuery('.' + pclass + ' .step' + stt).removeClass('hidden');
}
function err_tltt(pclass, sms) {
    if (sms) {
        jQuery('.' + pclass + ' #err_noted').html('* ' + sms);
    } else {
        jQuery('.' + pclass + ' #err_noted').html('');
    }
}
function change_action(obj, id) {
    var html_b = 'Chưa có đánh giá';
    var val_sel = jQuery(obj).val();
    switch (val_sel) {
        case '1':
            html_b = '<span>1 sao <i class="v2_icon_sao_1"></i></span> Kém';
            break;
        case '2':
            html_b = '<span>2 sao <i class="v2_icon_sao_2"></i></span> Trung bình';
            break;
        case '3':
            html_b = '<span>3 sao <i class="v2_icon_sao_3"></i></span> Cũng được';
            break;
        case '4':
            html_b = '<span>4 sao <i class="v2_icon_sao_4"></i></span> Khá tốt';
            break;
        case '5':
            html_b = '<span>5 sao <i class="v2_icon_sao_5"></i></span> Rất hài lòng';
            break;
    }
    jQuery("#change_text_des_" + id).html(html_b);
}


function validatePhone(phone) {
    var filter = /^(016([0-9]{1})|012([0-9]{1})|09[0-9]|08[0-9])(\d{7})$/i;
    if (!filter.test(phone)) {
        return false;
    }
    return true;
}


function Captcha() {
    var alpha = new Array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z');
    var i;
    for (i = 0; i < 6; i++) {
        var a = alpha[Math.floor(Math.random() * alpha.length)];
        var b = alpha[Math.floor(Math.random() * alpha.length)];
        var c = alpha[Math.floor(Math.random() * alpha.length)];
        var d = alpha[Math.floor(Math.random() * alpha.length)];
        var e = alpha[Math.floor(Math.random() * alpha.length)];
        var f = alpha[Math.floor(Math.random() * alpha.length)];
        var g = alpha[Math.floor(Math.random() * alpha.length)];
    }
    var code = a + ' ' + b + ' ' + ' ' + c + ' ' + d + ' ' + e + ' ' + f + ' ' + g;
    document.getElementById("mainCaptcha").innerHTML = code
}
function validateCaptcha() {
    var string1 = removeSpaces(document.getElementById('mainCaptcha').innerHTML);
    var string2 = removeSpaces(document.getElementById('com_captcha').value);
    if (string1 == string2) {
        return true;
    }
    else {
        return false;
    }
}
function removeSpaces(string) {
    return string.split(' ').join('');
}