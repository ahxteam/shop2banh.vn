﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjectManagement.Models
{
    public class GalleryImageModel
    {
        public int Id { get; set; }
        public int GalleryId { get; set; }
        public string Image { get; set; }
        public string Description { get; set; }
        //[Required(ErrorMessage ="Bạn chưa chọn ảnh")]
        public string FileBase { get; set; }
        public string Link { get; set; }
    }
}