﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectManagement.Models
{
    public class ProductInCategory
    {
        public Category Category { get; set; }
        public List<Product> Products { get; set; }
    }

    public class CateAndSubCate
    {
        public Category Category { get; set; }
        public List<Category> SubCategory { get; set; }
    }
}