﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjectManagement.Models
{
    public class CategoryModel
    {
        public int Id { get; set; }
        [Required(ErrorMessage ="Bạn chưa nhập tên danh mục")]
        [MaxLength(250, ErrorMessage ="Số ký tự tối đa là 250")]
        public string Name { get; set; }
        public bool Active { get; set; }
        public int OrderDisplay { get; set; }
        public string Description { get; set; }
        public int Parent { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string UserId { get; set; }
        public System.DateTime UpdatedDate { get; set; }
        public string UpdatedUser { get; set; }
        public Nullable<int> StatusId { get; set; }
        public string UrlCode { get; set; }
        public string Thumb { get; set; }
        public HttpPostedFileBase FileBase { get; set; }
    }
}