﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjectManagement.Models
{
    public class ProductModel
    {
        public int Id { get; set; }
        [Required(ErrorMessage ="Bạn chưa nhập tên")]
        [MaxLength(250,ErrorMessage ="Số ký tự tối đa là 250")]
        public string Name { get; set; }
        public int CategoryId { get; set; }
        public string UrlCode { get; set; }
        [Required(ErrorMessage = "Bạn chưa nhập description seo")]
        [MaxLength(250, ErrorMessage = "Số ký tự tối đa là 250")]
        public string Description { get; set; }
        [Required(ErrorMessage = "Bạn chưa nhập từ khóa")]
        [MaxLength(250, ErrorMessage = "Số ký tự tối đa là 250")]
        public string Keyword { get; set; }
        [Required(ErrorMessage = "Bạn chưa nhập chi tiết sản phẩm")]
        public string Detail { get; set; }
        [Required(ErrorMessage = "Bạn chưa nhập mã sản phẩm")]
        [MaxLength(250, ErrorMessage = "Số ký tự tối đa là 250")]
        public string ProductCode { get; set; }
        public string Original { get; set; }
        public string Unit { get; set; }
        public string Size { get; set; }
        public string Material { get; set; }
        public decimal Price { get; set; }
        public int View { get; set; }
        public string Summary { get; set; }
        public bool Active { get; set; }
        public int DisplayOrder { get; set; }
        public HttpPostedFileBase FileBase { get; set; }
        public string Thumb { get; set; }
        public string CategoryName { get; set; }
    }
}