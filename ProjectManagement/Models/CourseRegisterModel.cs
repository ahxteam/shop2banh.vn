﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjectManagement.Models
{
    public class CourseRegisterModel
    {
        public int Id { get; set; }
        public Nullable<int> CourseId { get; set; }
        [Required(ErrorMessage ="Bạn chưa nhập tên")]
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public string CourseName { get; set; }
    }
}