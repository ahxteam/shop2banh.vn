﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectManagement.Models
{
    public class RealStructModel
    {
        public int Id { get; set; }
        [Required(ErrorMessage ="Bạn chưa nhập tên giới thiệu")]
        [MaxLength(250,ErrorMessage ="Số ký tự tối đa là 250")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Bạn chưa nhập nội dung trang")]
        [AllowHtml]
        public string Content { get; set; }
        [Required(ErrorMessage = "Bạn chưa nhập mô tả trang")]
        [MaxLength(250, ErrorMessage = "Số ký tự tối đa là 250")]
        public string Description { get; set; }
        [Required(ErrorMessage = "Bạn chưa nhập từ khóa")]
        [MaxLength(250, ErrorMessage = "Số ký tự tối đa là 250")]
        public string Keyword { get; set; }
        public string DefaultImage { get; set; }
    }
}