﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectManagement.Models
{
    public class NotificationModel
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public int ReceivedUser { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ProfilePicture { get; set; }
        public string UserName { get; set; }
    }
}