﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectManagement.Models
{
    public class ProductGalleryModel
    {
        public int Id { get; set; }
        public Nullable<int> ProductId { get; set; }
        public string Thumb { get; set; }
        public string Title { get; set; }
        public HttpPostedFileBase FileBase { get; set; }
    }
}