﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectManagement.Models
{
    public class RateModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Captcha { get; set; }
        public int PriceRating { get; set; }
        public int PositionRating { get; set; }
        public int ServiceRating { get; set; }
        public int DesignRating { get; set; }
        public int InvestorRating { get; set; }
        public string Comment { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public int Likes { get; set; }
        public int RatingParent { get; set; }
        public string SessionId { get; set; }

       

    }
    public class RateAvg
    {
        public decimal PriceRatingAvg { get; set; }
        public decimal PositionRatingAvg { get; set; }
        public decimal ServiceRatingAvg { get; set; }
        public decimal DesignRatingAvg { get; set; }
        public decimal InvestorRatingAvg { get; set; }
        public decimal TotalAvg { get; set; }
        public int TotalCount { get; set; }
    }
    public class RateCommentModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Comment { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public int Likes { get; set; }
        public int RatingParent { get; set; }
        public string SessionId { get; set; }
        public int PriceRating { get; set; }
        public int PositionRating { get; set; }
        public int ServiceRating { get; set; }
        public int DesignRating { get; set; }
        public int InvestorRating { get; set; }
        public List<RateCommentModel> SubComments { get; set; }
        public decimal TotalAvg { get; set; }
        public int PageSize { get; set; }

    }
}