﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjectManagement.Models
{
    public class ApartmentModel
    {
        public int Id { get; set; }
        public int ApartmentTypeId { get; set; }
        [Required(ErrorMessage = "Bạn chưa nhập tên mặt bằng căn hộ")]
        [MaxLength(250, ErrorMessage = "Số ký tự tối đa là 250")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Bạn chưa nhập diện tích mặt bằng căn hộ")]
        [MaxLength(250, ErrorMessage = "Số ký tự tối đa là 250")]
        public string Acreage { get; set; }
        public string Thumb { get; set; }
        public int DisplayOrder { get; set; }
        public bool Active { get; set; }
        [Required(ErrorMessage = "Bạn chưa nhập mô tả")]
        [MaxLength(250, ErrorMessage = "Số ký tự tối đa là 250")]
        public string Description { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public HttpPostedFileBase FileBase { get; set; }
        public string ApartmentTypeName { get; set; }
    }
}