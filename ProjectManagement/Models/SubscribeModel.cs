﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjectManagement.Models
{
    public class SubscribeModel
    {
        public int Id { get; set; }
        [Required(ErrorMessage ="Bạn chưa nhập tên")]
        [MaxLength(50,ErrorMessage ="Số ký tự tối đa là 50")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Bạn chưa nhập email")]
        [MaxLength(50, ErrorMessage = "Số ký tự tối đa là 50")]
        [EmailAddress(ErrorMessage ="Email không hợp lệ")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Bạn chưa nhập số điện thoại")]
        [MaxLength(20, ErrorMessage = "Số ký tự tối đa là 20")]
        public string Phone { get; set; }
        [Required(ErrorMessage = "Bạn chưa nhập nội dung")]
        public string Content { get; set; }
        public System.DateTime CreatedDate { get; set; }
        [Required(ErrorMessage = "Bạn chưa nhập tiêu đề")]
        [MaxLength(250, ErrorMessage = "Số ký tự tối đa là 250")]
        public string Subject { get; set; }
    }
}