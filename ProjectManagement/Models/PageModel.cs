﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjectManagement.Models
{
    public class PageModel
    {
        public int Id { get; set; }
        [Required(ErrorMessage ="Bạn chưa nhập tên trang")]
        [MaxLength(250, ErrorMessage ="Số ký tự tối đa là 250")]
        public string Name { get; set; }
        public string UrlCode { get; set; }
        public bool Active { get; set; }
        public int OrderDisplay { get; set; }
        [MaxLength(250, ErrorMessage = "Số ký tự tối đa là 250")]
        public string Description { get; set; }
        public DateTime CreatedDate { get; set; }
        [Required(ErrorMessage = "Bạn chưa nhập nội dung")]
        public string Content { get; set; }
        public string ActiveStr { get; set; }
        [MaxLength(250, ErrorMessage = "Số ký tự tối đa là 250")]
        public string Keyword { get; set; }
        public int TemplateType { get; set; }
    }
}