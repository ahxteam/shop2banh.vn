﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectManagement.Models
{
    public class ProjectTaskModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int ProjectId { get; set; }
        public string UserId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int StatusId { get; set; }
        public string Description { get; set; }
        public int Progress { get; set; }
        public int CreatedUser { get; set; }
        public string UserName { get; set; }
        public string ManagerName { get; set; }
        public string ProjectName { get; set; }
    }
}