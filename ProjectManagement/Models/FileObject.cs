﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectManagement.Models
{
    public class FileObject
    {
        public string FilePath { get; set; }
        public string AbsolutePath { get; set; }
        public string FileName { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}