﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjectManagement.Models
{
    public class NewsModel
    {
        public int NewsID { get; set; }
        [Required(ErrorMessage ="Bạn chưa nhập tiêu đề")]
        [MaxLength(250,ErrorMessage = "Số ký tự tối đa 250")]
        public string Title { get; set; }
        public string UrlCode { get; set; }
        public string ThumbPic { get; set; }
        public Nullable<int> MenuID { get; set; }
        [Required(ErrorMessage = "Bạn chưa nhập từ khóa")]
        [MaxLength(250, ErrorMessage = "Số ký tự tối đa 250")]
        public string Keyword { get; set; }
        [Required(ErrorMessage = "Bạn chưa nhập description")]
        [MaxLength(250, ErrorMessage = "Số ký tự tối đa 250")]
        public string Description { get; set; }
        public Nullable<int> View { get; set; }
        public string Summary { get; set; }
        [Required(ErrorMessage = "Bạn chưa nhập nội dung bài viết")]
        public string Content { get; set; }
        public bool Active { get; set; }
        public Nullable<int> OrderDisplay { get; set; }
        public string UserID { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public string UpdatedUserID { get; set; }
        public Nullable<System.DateTime> UpdatedDate { get; set; }
        public Nullable<int> StatusID { get; set; }
        public string Tags { get; set; }
        public HttpPostedFileBase FileBase{ get; set; }
        public string MenuName { get; set; }
        public string ActiveStr { get; set; }
    }
}