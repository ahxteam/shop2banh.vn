﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjectManagement.Models
{
    public class ProgressModel
    {
        public int Id { get; set; }
        public System.DateTime DateProgress { get; set; }
        [Required(ErrorMessage = "Bạn chưa nhập tên tiến độ")]
        [MaxLength(250, ErrorMessage = "Số ký tự tối đa là 250")]
        public string Name { get; set; }
        public System.DateTime CreatedDate { get; set; }
        [Required(ErrorMessage = "Bạn chưa nhập tiêu đề tiến độ")]
        [MaxLength(250, ErrorMessage = "Số ký tự tối đa là 250")]
        public string Title { get; set; }

        [Required(ErrorMessage = "Bạn chưa nhập description cho SEO")]
        [MaxLength(250, ErrorMessage = "Số ký tự tối đa là 250")]
        public string Description { get; set; }
        [Required(ErrorMessage = "Bạn chưa nhập keyword cho SEO")]
        [MaxLength(250, ErrorMessage = "Số ký tự tối đa là 250")]
        public string Keyword { get; set; }
        [Required(ErrorMessage = "Bạn chưa nhập thông tin tiến độ")]
        [MaxLength(1000, ErrorMessage = "Số ký tự tối đa là 1000")]
        public string Information { get; set; }
        public string UrlCode { get; set; }
    }
}