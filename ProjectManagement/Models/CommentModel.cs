﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjectManagement.Models
{
    public class CommentModel
    {
        public int Id { get; set; }
        [Required(ErrorMessage ="Bạn chưa nhập tên")]
        [MaxLength(250,ErrorMessage ="Độ dài lớn nhất là 250 ký tự")]
        public string Name { get; set; }
        public string Image { get; set; }
        [Required(ErrorMessage = "Bạn chưa nhập nội dung")]
        [MaxLength(1500, ErrorMessage = "Độ dài lớn nhất là 1500 ký tự")]
        public string Content { get; set; }
        public bool Active { get; set; }
        public HttpPostedFileBase FileBase { get; set; }
        public int Type { get; set; }
    }
}