﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjectManagement.Models
{
    public class VideoModel
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Bạn chưa nhập tên")]
        [MaxLength(250, ErrorMessage = "Số ký tự tối đa 250")]
        public string Name { get; set; }
        public string CodeEmbeb { get; set; }
        [Required(ErrorMessage = "Bạn chưa nhập mô tả")]
        [MaxLength(250, ErrorMessage = "Số ký tự tối đa 250")]
        public string Description { get; set; }
        public System.DateTime CreatedDate { get; set; }
        [Required(ErrorMessage = "Bạn chưa nhập nội dung")]
        public string Content { get; set; }
        public string Thumb { get; set; }
        [Required(ErrorMessage = "Bạn chưa nhập từ khóa")]
        [MaxLength(250, ErrorMessage = "Số ký tự tối đa 250")]
        public string Keyword { get; set; }
        public string UrlCode { get; set; }
        public bool Active { get; set; }
        public HttpPostedFileBase FileBase { get; set; }
        [MaxLength(1000, ErrorMessage = "Số ký tự tối đa 1000")]
        public string Summary { get; set; }
    }
}