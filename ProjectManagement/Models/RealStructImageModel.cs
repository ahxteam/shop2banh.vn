﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectManagement.Models
{
    public class RealStructImageModel
    {
        public int Id { get; set; }
        public Nullable<int> RealStructId { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
        public HttpPostedFileBase FileBase { get; set; }
    }
}