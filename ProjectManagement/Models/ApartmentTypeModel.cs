﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjectManagement.Models
{
    public class ApartmentTypeModel
    {
        public int Id { get; set; }
        public string Thumb { get; set; }
        [Required(ErrorMessage ="Bạn chưa nhập tên loại")]
        [MaxLength(250,ErrorMessage ="Số ký tự tối đa là 250")]
        public string TypeName { get; set; }
        [Required(ErrorMessage = "Bạn chưa nhập tên kiểu căn hộ")]
        [MaxLength(250, ErrorMessage = "Số ký tự tối đa là 250")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Bạn chưa nhập mô tả diện tích")]
        [MaxLength(250, ErrorMessage = "Số ký tự tối đa là 250")]
        public string Acreage { get; set; }
        public int DisplayOrder { get; set; }
        public bool Active { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public HttpPostedFileBase FileBase { get; set; }
    }
}