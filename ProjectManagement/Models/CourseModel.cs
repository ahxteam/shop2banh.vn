﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectManagement.Models
{
    public class CourseModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Nullable<int> ParentId { get; set; }
        public string UrlCode { get; set; }
        public int OrderDisplay { get; set; }
        public bool Active { get; set; }
        public string Summary { get; set; }
        public string ThumbPic { get; set; }
        public HttpPostedFileBase FileBase { get; set; }
    }
}