﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjectManagement.Models
{
    public class PositionModel
    {
        [Required]
        public int Id { get; set; }
        [Required (ErrorMessage ="Bạn chưa nhập tên chức vụ")]
        [MaxLength(250, ErrorMessage ="Số ký tự tối đa là 250")]
        public string Name { get; set; }
        [MaxLength(250,ErrorMessage = "Số ký tự tối đa là 250")]
        public string Description { get; set; }
        public string RoleId { get; set; }
    }
}