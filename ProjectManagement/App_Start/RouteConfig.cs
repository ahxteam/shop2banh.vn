﻿using ProjectManagement.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ProjectManagement
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.LowercaseUrls = true;
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );

            // page
            //routes.Add("PageDetail", new SeoFriendlyRoute("page/detail/{id}",
            //new RouteValueDictionary(new { controller = "Page", action = "Detail" }),
            //new MvcRouteHandler()));
            routes.MapRoute("Page", // Route name
                   "{urlCode}/{controller}/{action}/{id}/",
                   new
                   {
                       controller = "Page",
                       action = "Detail",
                       id = "",
                       urlCode = ""
                   }  // Parameter defaults )
                   );

            routes.MapRoute("Course", // Route name
              "{urlCode}/{controller}/{action}",
              new
              {
                  controller = "CourseView",
                  action = "Index",
                  urlCode = ""
              }  // Parameter defaults )
              );

            routes.MapRoute("SubCourse", // Route name
              "{urlCode}/{controller}/{action}/{id}/",
              new
              {
                  controller = "CourseView",
                  action = "SubCourse",
                  id = "",
                  urlCode = ""
              } 
              );
            routes.MapRoute("CourseDetail", 
              "{urlCode}/{controller}/{action}/{id}/",
              new
              {
                  controller = "CourseView",
                  action = "SubCourse",
                  id = "",
                  urlCode = ""
              }
              );

            // news view
            routes.MapRoute("NewsView", // Route name
              "{urlCode}/{controller}/{action}",
              new
              {
                  controller = "NewsView",
                  action = "Index",
                  urlCode = ""
              }  // Parameter defaults )
              );

            routes.MapRoute("SubNews", // Route name
              "{urlCode}/{controller}/{action}/{id}/",
              new
              {
                  controller = "NewsView",
                  action = "SubNews",
                  id = "",
                  urlCode = ""
              }
              );
            routes.MapRoute("NewsDetail",
              "{urlCode}/{controller}/{action}/{id}/",
              new
              {
                  controller = "NewsView",
                  action = "Detail",
                  id = "",
                  urlCode = ""
              }
              );
        }
    }
}
