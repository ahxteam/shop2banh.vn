﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectManagement.Controllers
{
    [RoutePrefix("trang")]
    public class PageController : Controller
    {
        private ProjectManagementEntities entities = new ProjectManagementEntities();
        // GET: Pages
        public ActionResult Index()
        {
            return View();
        }
        [Route("{id}/{url}")]
        public ActionResult Detail(int id, string url)
        {
            var page = entities.Pages.FirstOrDefault(o => o.Id == id);
            return View(page);
        }

    }
}