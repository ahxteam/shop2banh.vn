﻿using AutoMapper;
using Model;
using ProjectManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectManagement.Controllers
{
    public class ContactController : Controller
    {
        private ProjectManagementEntities entities = new ProjectManagementEntities();
        // GET: Contact
        [Route("lien-he", Name = "ContactIndex")]
        public ActionResult Index()
        {
            var model = new SubscribeModel();
            return View(model);
        }
        public ActionResult Index(SubscribeModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            else
            {
                model.CreatedDate = DateTime.Now;
                var entity = entities.Subscribes.AsNoTracking().FirstOrDefault(o => o.Id == model.Id);
                Mapper.CreateMap<SubscribeModel, Subscribe>();
                entity = Mapper.Map<SubscribeModel, Subscribe>(model);
                if (entity != null && entity.Id > 0)
                {
                    entities.Entry(entity).State = System.Data.Entity.EntityState.Modified;
                }
                else
                {
                    entities.Entry(entity).State = System.Data.Entity.EntityState.Added;
                }
                try
                {
                    entities.SaveChanges();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                return RedirectPermanent("/");
            }
        }
    }
}