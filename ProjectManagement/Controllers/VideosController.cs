﻿using Model;
using PagedList;
using ProjectManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectManagement.Controllers
{
    public class VideosController : Controller
    {
        ProjectManagementEntities entities = new ProjectManagementEntities();
        // GET: Course
        [Route("video-thi-cong", Name = "VideosIndex")]
        public ViewResult Index(string sortOrder, string currentFilter, string searchString, int? page)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.DateSortParm = sortOrder == "Date" ? "date_desc" : "Date";

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;

            var entity = entities.Videos.ToList();
            if (!string.IsNullOrEmpty(searchString))
            {
                entity = entity.Where(s => s.Name.Contains(searchString)
                                       || s.Name.Contains(searchString)).ToList();
            }
            switch (sortOrder)
            {
                case "title_desc":
                    entity = entity.OrderByDescending(s => s.Name).ToList();
                    break;
                case "created_desc":
                    entity = entity.OrderBy(s => s.CreatedDate).ToList();
                    break;
                case "view_desc":
                    entity = entity.OrderByDescending(s => s.Name).ToList();
                    break;
                default:
                    entity = entity.OrderByDescending(s => s.CreatedDate).ToList();
                    break;
            }

            int pageSize = 10;
            int pageNumber = (page ?? 1);

            var modelList = entity.Select(o => new VideoModel()
            {
                CreatedDate = o.CreatedDate,
                Id = o.Id,
                Name = o.Name,
                Content = o.Content,
                UrlCode = o.UrlCode,
                Thumb = o.Thumb,
                Summary = o.Summary
            }).ToList();
            return View(modelList.ToPagedList(pageNumber, pageSize));
        }
        [Route("video-thi-cong/{id}/{url}", Name = "VideosDetail")]
        public ActionResult Detail(int id, string url)
        {
            var model = entities.Videos.FirstOrDefault(o => o.Id == id && o.Active == true);
            return View(model);
        }
    }
}