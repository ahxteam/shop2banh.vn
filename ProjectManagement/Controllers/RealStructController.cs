﻿using Model;
using ProjectManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectManagement.Controllers
{
    public class RealStructController : Controller
    {
        private ProjectManagementEntities entities = new ProjectManagementEntities();

        // GET: RealStruct
        [Route("cong-trinh-thuc-te", Name ="RealStructIndex")]
        public ActionResult Index()
        {
            var gallery = entities.RealStructs.ToList();
            var listData = new List<RealStructModel>();
            foreach (var item in gallery)
            {
                listData.Add(new RealStructModel()
                {
                    DefaultImage = entities.RealStructImages.FirstOrDefault(o => o.RealStructId == item.Id)!=null ? entities.RealStructImages.FirstOrDefault(o => o.RealStructId == item.Id).Image : string.Empty,
                    Name = item.Name,
                    Id = item.Id
                });
            }
            ViewBag.Data = listData;
            return View();
        }
        [Route("cong-trinh-thuc-te/{id}")]
        public ActionResult Detail(int id)
        {
            var data =entities.RealStructImages.Where(o => o.RealStructId == id).ToList();
            data = data.Select(o => { o.Image = o.Image.Replace(" ", "%20"); return o; }).ToList();
            ViewBag.RealStruct = entities.RealStructs.FirstOrDefault(o => o.Id == id);
            ViewBag.Data = data;
            return View();
        }
    }
}