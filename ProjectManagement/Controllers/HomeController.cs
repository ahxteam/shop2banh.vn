﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Common;
using ProjectManagement.Models;
using System.Data.Entity.Core.Objects;
using System.Data.Entity;
using Newtonsoft.Json;
using ProjectManagement.Helper;

namespace ProjectManagement.Controllers
{
    public class HomeController : Controller
    {
        private ProjectManagementEntities entities = new ProjectManagementEntities();
        public ActionResult Index()
        {
            //var listCateNews = entities.Menus.Where(o => o.Active == true).OrderByDescending(o=>o.OrderDisplay).Take(8).ToList();
            //var listDataNews = new Dictionary<Menu,List<News>>();
            //foreach (var item in listCateNews)
            //{
            //    var listNews = entities.News.Where(o => o.Active == true && o.MenuID== item.MenuId).OrderByDescending(o=>o.OrderDisplay).Take(5).ToList();
            //    listDataNews.Add(item, listNews);
            //}
            //ViewBag.ListNews = listDataNews;

            // menu
            var listMenuCate = new List<CateAndSubCate>();
            var menuCate = entities.Categories.Where(o => o.Active == true).ToList();
            foreach (var item in menuCate)
            {
                listMenuCate.Add(new CateAndSubCate()
                {
                    Category = item,
                    SubCategory = entities.Categories.Where(o => o.Active == true && o.Parent == item.Id).OrderByDescending(o => o.OrderDisplay).ToList()
                });
            }
            ViewBag.ListMenuCate = listMenuCate;

            // gallery
            var galleryId = entities.Galleries.FirstOrDefault(o => o.Active == true && o.Type == ((int)EnumTypeGallery.Slider)).Id;
            var listGalleryImages = entities.GalleryImages.Where(o => o.GalleryId == galleryId).OrderByDescending(o => o.Id).ToList();
            ViewBag.GalleryImages = listGalleryImages;


            // new product
            var listNewProducts = entities.Products.Where(o => o.Active == true).OrderByDescending(o => o.Id).Take(8).ToList();
            ViewBag.ListNewProducts = listNewProducts;

            // favourite product
            var listFavouriteProducts = entities.Products.Where(o => o.Active == true).OrderByDescending(o => o.View).Take(8).ToList();
            ViewBag.ListFavouriteProducts = listFavouriteProducts;

            // favourite product
            var listProductInCategory = new List<ProductInCategory>();
            var cates = entities.Categories.Where(o => o.StatusId == 1 && o.Active==true).ToList();
            foreach (var item in cates)
            {
                listProductInCategory.Add(new ProductInCategory()
                {
                    Category = item,
                    Products = entities.Products.Where(o => o.Active == true && o.CategoryId == item.Id).OrderByDescending(o => o.View).Take(8).ToList()
                });
            }
            ViewBag.ListProductInCategory = listProductInCategory;

            // new news
            var listNewNews = entities.News.Where(o => o.Active == true).OrderByDescending(o => o.NewsID).Take(8).ToList();
            ViewBag.ListNewNews = listNewNews;

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        [Route("ban-do")]
        public ActionResult Map()
        {
            return View();
        }
    }
}