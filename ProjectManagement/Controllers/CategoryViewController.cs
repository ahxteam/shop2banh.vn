﻿using AutoMapper;
using Model;
using PagedList;
using ProjectManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectManagement.Controllers
{
    public class CategoryViewController : Controller
    {
        ProjectManagementEntities entities = new ProjectManagementEntities();
        // GET: Category

        //[Route("danh-muc/{id}/{url}")]
        //public ActionResult Index(int id, string url)
        //{
        //    var listProductInCategory = new ProductInCategory();
        //    var cates = entities.Categories.Where(o=>o.Id == id && o.Active == true).ToList();
        //    foreach (var item in cates)
        //    {
        //        listProductInCategory.Category = item;
        //        listProductInCategory.Products = entities.Products.Where(o => o.Active == true && o.CategoryId == item.Id).OrderByDescending(o => o.View).Take(8).ToList();
        //    }
        //    ViewBag.ListProducts = listProductInCategory;
        //    return View();
        //}
        [Route("danh-muc/{id}/{url}")]
        public ViewResult Index(int id, string url, int page = 1, string sortOrder = "created_desc", int pageSize = 12)
        {
            string sort = string.Empty;
            ViewBag.PageSize = pageSize;

            var product = entities.Products.Where(o => o.CategoryId == id && o.Active == true).ToList();
            switch (sortOrder)
            {
                case "created_desc":
                    product = product.OrderByDescending(s => s.Id).ToList();
                    sort = "Mới nhất";
                    break;
                case "created_asc":
                    product = product.OrderBy(s => s.Id).ToList();
                    sort = "Cũ nhất";
                    break;
                case "view_desc":
                    product = product.OrderByDescending(s => s.View).ToList();
                    sort = "Phổ biến";
                    break;
                case "name_desc":
                    product = product.OrderByDescending(s => s.Name).ToList();
                    break;
                default:
                    product = product.OrderByDescending(s => s.Id).ToList();
                    sort = "Mới nhất";
                    break;
            }
            ViewBag.SortParam = sort;

            int pageNumber = page;

            var modelList = product.Select(o => new ProductModel()
            {
                Id = o.Id,
                CategoryId = o.CategoryId,
                Name = o.Name,
                Summary = o.Summary,
                UrlCode = o.UrlCode,
                Thumb = o.Thumb,
                Price = o.Price
            }).ToList();

            var category = entities.Categories.FirstOrDefault(o => o.Id == id);

            ViewBag.Title = category.Name;
            ViewBag.Description = category.Description;
            ViewBag.Keyword = category.Name;
            ViewBag.Category = category;
            ViewBag.TotalProduct = product.Count;

            return View(modelList.ToPagedList(pageNumber, pageSize));
        }

        [Route("san-pham/{id}/{url}")]
        public ActionResult ProductDetail(int id, string url)
        {
            var product = entities.Products.FirstOrDefault(o => o.Id == id && o.Active == true);

            product.View = ++product.View;
            entities.Entry(product).State = System.Data.Entity.EntityState.Modified;
            entities.SaveChanges();

            ViewBag.ListImages = entities.ProductGalleries.Where(o => o.ProductId == id).ToList();
            Mapper.CreateMap<Product, ProductModel>();
            var model = Mapper.Map<Product, ProductModel>(product);

            ViewBag.Title = model.Name;
            ViewBag.Description = model.Description;
            ViewBag.Keyword = model.Keyword;
            ViewBag.Category = product.Category;

            return View(model);
        }

        [HttpPost]
        [Route("tim-kiem")]
        public ViewResult Search(string searchText = "", int page = 1)
        {
            string sort = string.Empty;
            ViewBag.SearchText = searchText;
            var product = new List<Product>();
            product = entities.Products.Where(o => o.Active == true && o.Name.Contains(searchText)).ToList();
            var modelList = product.OrderByDescending(o => o.View).Select(o => new ProductModel()
            {
                Id = o.Id,
                CategoryId = o.CategoryId,
                Name = o.Name,
                Summary = o.Summary,
                UrlCode = o.UrlCode,
                Thumb = o.Thumb,
                Price = o.Price
            }).ToList();

            ViewBag.Title = "Kết quả tìm kiếm";
            ViewBag.Description = searchText;
            ViewBag.Keyword = searchText;
            ViewBag.Total = modelList.Count;

            return View(modelList.ToPagedList(page, 12));
        }
    }
}