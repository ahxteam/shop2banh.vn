﻿using Model;
using PagedList;
using ProjectManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectManagement.Controllers
{
    [RoutePrefix("tin-tuc")]
    public class NewsViewController : Controller
    {
        ProjectManagementEntities entities = new ProjectManagementEntities();
        // GET: Course
        [Route("index", Name ="NewsViewIndex")]
        public ViewResult Index(string sortOrder, string currentFilter, string searchString, int? page)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.DateSortParm = sortOrder == "Date" ? "date_desc" : "Date";

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;

            var news = entities.News.ToList();
            if (!String.IsNullOrEmpty(searchString))
            {
                news = news.Where(s => s.Title.Contains(searchString)
                                       || s.Title.Contains(searchString)).ToList();
            }
            switch (sortOrder)
            {
                case "title_desc":
                    news = news.OrderByDescending(s => s.Title).ToList();
                    break;
                case "created_desc":
                    news = news.OrderBy(s => s.CreatedDate).ToList();
                    break;
                case "view_desc":
                    news = news.OrderByDescending(s => s.View).ToList();
                    break;
                default:
                    news = news.OrderByDescending(s => s.CreatedDate).ToList();
                    break;
            }

            int pageSize = 10;
            int pageNumber = (page ?? 1);

            var newsModelList = news.Select(o => new NewsModel()
            {
                CreatedDate = o.CreatedDate,
                MenuID = o.MenuID,
                NewsID = o.NewsID,
                MenuName = entities.Menus.FirstOrDefault(s => s.MenuId == o.MenuID)?.Name,
                Summary = o.Summary,
                Tags = o.Tags,
                UrlCode = o.UrlCode,
                ThumbPic = o.ThumbPic,
                Title = o.Title,
            }).ToList();

            
            ViewBag.Description = Helper.ResourceHelper.GetResourceByKey("TitleSite");
            ViewBag.Keyword = Helper.ResourceHelper.GetResourceByKey("TitleSite");

            return View(newsModelList.ToPagedList(pageNumber, pageSize));
        }
     
        [Route("chi-tiet/{id}/{url}", Name = "NewsViewDetail")]
        public ActionResult Detail(int id, string url)
        {
            var model = entities.News.FirstOrDefault(o => o.NewsID == id && o.Active == true);
            ViewBag.Parent = entities.Menus.FirstOrDefault(o => o.MenuId == model.MenuID && o.Active == true);
            ViewBag.RelatedNews = entities.News.Where(o => o.MenuID == model.MenuID && o.Active == true).OrderByDescending(o => o.CreatedDate).Take(5).ToList();


            model.View = ++model.View;
            entities.Entry(model).State = System.Data.Entity.EntityState.Modified;
            entities.SaveChanges();
            ViewBag.Title = model.Title;
            ViewBag.Description = model.Description;
            ViewBag.Keyword = model.Keyword;

            return View(model);
        }
    }
}