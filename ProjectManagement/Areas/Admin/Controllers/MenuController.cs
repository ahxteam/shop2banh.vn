﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using Model;
using ProjectManagement.Helper;
using ProjectManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectManagement.Areas.Admin.Controllers
{
    public class MenuController : Controller
    {
        private ProjectManagementEntities entities = new ProjectManagementEntities();

        public ActionResult Index()
        {
            ViewBag.Menus = entities.Menus.Where(o => o.Parent == 0).OrderByDescending(o => o.OrderDisplay).ToList();
            return View();
        }
        public ActionResult SubMenu(int id)
        {
            ViewBag.Menus = entities.Menus.Where(o => o.Parent == id).OrderByDescending(o => o.OrderDisplay).ToList();
            return View();
        }
        #region CRUD
        [Authorize]
        [HttpGet]
        public ActionResult CreateOrEdit(int id = 0)
        {
            var model = new MenuModel()
            {
                MenuId = 0,
                Description = string.Empty,
                UrlCode = string.Empty,
                Name = string.Empty,
                OrderDisplay = 0,
                Parent = id
            };
            if (id > 0)
            {
                var resource = entities.Menus.FirstOrDefault(o => o.MenuId == id);
                Mapper.CreateMap<Menu, MenuModel>();
                model = Mapper.Map<Menu, MenuModel>(resource);
            }
            ViewBag.Menus = entities.Menus.Where(o => o.MenuId != id).ToList();
            return PartialView(model);
        }

        [Authorize]
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CreateOrEdit(MenuModel model)
        {
            if (!ModelState.IsValid)
            {
                return Json(new
                {
                    result = false,
                    Errors =
                    ModelState.Keys.Where(
                        p => ModelState[p].Errors.Any(e => string.IsNullOrEmpty(e.ErrorMessage) == false)).
                        Select(p => new
                        {
                            field = p,
                            msgs = ModelState[p].Errors.Select(e => e.ErrorMessage)
                        })
                });
            }
            else
            {
                model.CreatedDate = DateTime.Now;
                model.UpdatedDate = DateTime.Now;
                model.UserId = User.Identity.GetUserId();
                if (string.IsNullOrEmpty(model.UrlCode))
                {
                    model.UrlCode = UtilHelper.RejectMarks(model.Name);
                }
                else
                {
                    model.UrlCode = UtilHelper.RejectMarks(model.UrlCode);
                }
                if (model.OrderDisplay <= 0)
                {
                    model.OrderDisplay = entities.Menus.Max(o => o.OrderDisplay) + 1;
                }
                var menu = entities.Menus.AsNoTracking().FirstOrDefault(o => o.MenuId == model.MenuId);
                var subMenu = entities.Menus.AsNoTracking().Where(o => o.Parent == o.MenuId).ToList();
                subMenu = subMenu.Select(s =>
                {
                    s.Active = model.Active;
                    return s;
                }).ToList();

                Mapper.CreateMap<MenuModel, Menu>();
                menu = Mapper.Map<MenuModel, Menu>(model);
                if (menu != null && menu.MenuId > 0)
                {
                    entities.Entry(menu).State = System.Data.Entity.EntityState.Modified;
                }
                else
                {
                    entities.Entry(menu).State = System.Data.Entity.EntityState.Added;
                }
                try
                {
                    entities.SaveChanges();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                return Json(new { result = true });
            }
        }
        [HttpPost]
        public ActionResult Delete(int id = 0)
        {
            if (id > 0)
            {
                var menu = entities.Menus.FirstOrDefault(o => o.MenuId == id);
                entities.Menus.RemoveRange(entities.Menus.Where(o => o.Parent == menu.MenuId));
                entities.Menus.Remove(menu);
                entities.SaveChanges();
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            return Json(false, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}