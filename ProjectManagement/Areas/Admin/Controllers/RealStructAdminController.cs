﻿using AutoMapper;
using Model;
using ProjectManagement.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectManagement.Areas.Admin.Controllers
{
    public class RealStructAdminController : Controller
    {
        private ProjectManagementEntities entities = new ProjectManagementEntities();

        public ActionResult Index()
        {
            ViewBag.Data = entities.RealStructs.OrderByDescending(o => o.Id).ToList();
            return View();
        }

        #region CRUD
        [Authorize]
        [HttpGet]
        public ActionResult CreateOrEdit(int id = 0)
        {
            var model = new RealStructModel();
            if (id > 0)
            {
                var entity = entities.RealStructs.FirstOrDefault(o => o.Id == id);
                Mapper.CreateMap<RealStruct, RealStructModel>();
                model = Mapper.Map<RealStruct, RealStructModel>(entity);
            }
            return View(model);
        }

        [Authorize]
        [HttpPost]
        public ActionResult CreateOrEdit(RealStructModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            else
            {
                var entity = entities.RealStructs.AsNoTracking().FirstOrDefault(o => o.Id == model.Id);
                Mapper.CreateMap<RealStructModel, RealStruct>();
                entity = Mapper.Map<RealStructModel, RealStruct>(model);
                if (entity != null && entity.Id > 0)
                {
                    entities.Entry(entity).State = System.Data.Entity.EntityState.Modified;
                }
                else
                {
                    entities.Entry(entity).State = System.Data.Entity.EntityState.Added;
                }
                try
                {
                    entities.SaveChanges();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                return RedirectPermanent("/admin/realstructadmin/index");
            }
        }

        [HttpPost]
        public ActionResult Delete(int id = 0)
        {
            if (id > 0)
            {
                var entity = entities.RealStructs.FirstOrDefault(o => o.Id == id);
                entities.RealStructs.Remove(entity);
                entities.SaveChanges();
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            return Json(false, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region  Gallery
        public ActionResult GalleryImage(int id)
        {
            var data = entities.RealStructImages.Where(o => o.RealStructId == id).OrderByDescending(o => o.Id).ToList();
            ViewBag.GalleryImages = data;
            return View(new RealStructImageModel() { RealStructId = id });
        }

        [Authorize]
        [HttpPost]
        public ActionResult GalleryImage(RealStructImageModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            else
            {
                if (model.FileBase != null && model.FileBase.ContentLength > 0)
                {
                    var fileName = Path.GetFileName(model.FileBase.FileName);
                    fileName = string.Format("{0:yyyyMMddhhmmss}_{1}", DateTime.Now, $"{DateTime.Now.ToString("yyyyMMddHHmmss")} {fileName}");
                    var path = Path.Combine(Server.MapPath("~/Uploads/Images"), fileName);
                    model.FileBase.SaveAs(path);
                    model.Image = Path.Combine("/Uploads/Images", fileName);
                }
                else
                {
                    model.Image = "";
                }
                var galleryImage = entities.RealStructImages.AsNoTracking().FirstOrDefault(o => o.Id == model.Id);
                Mapper.CreateMap<RealStructImageModel, RealStructImage>();
                galleryImage = Mapper.Map<RealStructImageModel, RealStructImage>(model);
                if (galleryImage != null && galleryImage.Id > 0)
                {
                    entities.Entry(galleryImage).State = System.Data.Entity.EntityState.Modified;
                }
                else
                {
                    entities.Entry(galleryImage).State = System.Data.Entity.EntityState.Added;
                }
                try
                {
                    entities.SaveChanges();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return RedirectPermanent("/admin/realstructadmin/galleryimage/" + model.RealStructId);
        }
        [HttpPost]
        public ActionResult DeleteImage(int id = 0)
        {
            int realStructId = 0;
            if (id > 0)
            {
                var e = entities.RealStructImages.FirstOrDefault(o => o.Id == id);
                realStructId = e.Id;
                entities.RealStructImages.Remove(e);
                entities.SaveChanges();

            }
            if (realStructId > 0)
            {
                return RedirectPermanent("/admin/realstructadmin/galleryimage/" + realStructId);
            }
            else
            {
                return RedirectPermanent("/admin/realstructadmin/index");
            }
        }
        #endregion
    }
}