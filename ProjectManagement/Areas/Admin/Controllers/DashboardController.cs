﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Common;
using ProjectManagement.Models;
using System.Data.Entity.Core.Objects;
using System.Data.Entity;
using Newtonsoft.Json;

namespace ProjectManagement.Areas.Admin.Controllers
{
    [Authorize]
    public class DashboardController : Controller
    {
        private ProjectManagementEntities entities = new ProjectManagementEntities();
        public ActionResult Index()
        {
            if (Request.IsAuthenticated)
            {
                var isAdmin = User.IsInRole("Admin");

                ViewBag.TotalNews = entities.News.Count();
                ViewBag.TotalSubscriber = entities.Subscribes.Count();
                ViewBag.TotalProduct = entities.Products.Count();
                //// list users.
                var listProduct = entities.Products.Take(10).OrderByDescending(o=>o.Id).ToList();

                ViewBag.NewProducts = listProduct.Select(o => new ProductModel()
                {
                    Active = o.Active,
                    CategoryName = entities.Categories.FirstOrDefault(m => m.Id == o.CategoryId).Name,
                    Thumb = o.Thumb,
                    Name = o.Name,
                    View = o.View
                }).ToList();

                ViewBag.NewContacts = entities.Subscribes.OrderByDescending(o => o.CreatedDate).Take(10).ToList();
                ViewBag.NewNews = entities.News.OrderByDescending(o => o.UpdatedDate.Value).Take(10).ToList();
                return View();
            }
            else
            {
                return Redirect("/admin/account/login");
            }
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}