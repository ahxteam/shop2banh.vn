﻿using AutoMapper;
using Model;
using PagedList;
using ProjectManagement.Helper;
using ProjectManagement.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectManagement.Areas.Admin.Controllers
{
    public class ProductController : Controller
    {
        // GET: Admin/Product
        private ProjectManagementEntities entities = new ProjectManagementEntities();

        public ViewResult Index(string sortOrder, string currentFilter, string searchString, int? page)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.DateSortParm = sortOrder == "Date" ? "date_desc" : "Date";

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;

            var entity = entities.Products.ToList();
            if (!String.IsNullOrEmpty(searchString))
            {
                entity = entity.Where(s => s.Name.Contains(searchString)
                                       || s.Name.Contains(searchString)).ToList();
            }
            switch (sortOrder)
            {
                case "title_desc":
                    entity = entity.OrderByDescending(s => s.Name).ToList();
                    break;
                case "view_desc":
                    entity = entity.OrderByDescending(s => s.View).ToList();
                    break;
                default:
                    entity = entity.OrderBy(s => s.Name).ToList();
                    break;
            }

            int pageSize = 25;
            int pageNumber = (page ?? 1);

            var entityModelList = entity.Select(o => new ProductModel()
            {
                Active = o.Active,
                Unit =o.Unit,
                CategoryId = o.CategoryId,
                Id  = o.Id,
                CategoryName = entities.Categories.FirstOrDefault(s => s.Id == o.CategoryId).Name,
                Summary = o.Summary,
                DisplayOrder = o.DisplayOrder,
                ProductCode = o.ProductCode,
                Thumb = o.Thumb,
                Name = o.Name,
                View = o.View
            }).ToList();

            return View(entityModelList.ToPagedList(pageNumber, pageSize));
        }

        #region CRUD
        [Authorize]
        [HttpGet]
        public ActionResult CreateOrEdit(int id = 0)
        {
            ViewBag.Categories = entities.Categories.Where(o=>o.Active==true).ToList();
            var model = new ProductModel()
            {
                Id = id
            };
            if (id > 0)
            {
                var entity = entities.Products.FirstOrDefault(o => o.Id == id);
                Mapper.CreateMap<Product, ProductModel>();
                model = Mapper.Map<Product, ProductModel>(entity);
            }
            return View(model);
        }

        [Authorize]
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CreateOrEdit(ProductModel model)
        {
            ViewBag.Categories = entities.Categories.Where(o=>o.Active==true).ToList();
            model.View = 0;
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            else
            {
                if (model.DisplayOrder <= 0)
                {
                    model.DisplayOrder = entities.Products.Max(o => o.DisplayOrder) + 1;
                }
                if (model.FileBase != null && model.FileBase.ContentLength > 0)
                {
                    var fileName = Path.GetFileName(model.FileBase.FileName);
                    var path = Path.Combine(Server.MapPath("~/Uploads/Images"), fileName);
                    model.FileBase.SaveAs(path);
                    model.Thumb = Path.Combine("/Uploads/Images", fileName);
                }

                if (string.IsNullOrEmpty(model.UrlCode))
                {
                    model.UrlCode = UtilHelper.RejectMarks(model.Name);
                }
                else
                {
                    model.UrlCode = UtilHelper.RejectMarks(model.UrlCode);
                }

                if (string.IsNullOrEmpty(model.Description))
                {
                    model.Description = model.Name;

                }

                var entity = entities.Products.AsNoTracking().FirstOrDefault(o => o.Id == model.Id);
                Mapper.CreateMap<ProductModel, Product>();
                entity = Mapper.Map<ProductModel, Product>(model);
                if (entity != null && entity.Id > 0)
                {
                    entities.Entry(entity).State = System.Data.Entity.EntityState.Modified;
                }
                else
                {
                    entities.Entry(entity).State = System.Data.Entity.EntityState.Added;
                }
                try
                {
                    entities.SaveChanges();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                return RedirectPermanent("/admin/product/index");
            }

        }

        [HttpPost]
        public ActionResult Delete(int id = 0)
        {
            if (id > 0)
            {
                entities.Products.Remove(entities.Products.FirstOrDefault(o => o.Id == id));
                entities.SaveChanges();
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            return Json(false, JsonRequestBehavior.AllowGet);
        }
        #endregion


        #region Product Gallery
        public ActionResult Gallery(int id)
        {
            var data = entities.ProductGalleries.Where(o => o.ProductId == id).OrderByDescending(o => o.Id).ToList();
            ViewBag.GalleryImages = data;
            return View(new ProductGalleryModel() { ProductId = id });
        }

        [Authorize]
        [HttpPost]
        public ActionResult Gallery(ProductGalleryModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            else
            {
                if (model.FileBase != null && model.FileBase.ContentLength > 0)
                {
                    var fileName = Path.GetFileName(model.FileBase.FileName);
                    fileName = string.Format("{0:yyyyMMddhhmmss}_{1}", DateTime.Now, $"{DateTime.Now.ToString("yyyyMMddHHmmss")} {fileName}");
                    var path = Path.Combine(Server.MapPath("~/Uploads/Images"), fileName);
                    model.FileBase.SaveAs(path);
                    model.Thumb = Path.Combine("/Uploads/Images", fileName);
                }
                else
                {
                    model.Thumb = "";
                }
                var galleryImage = entities.ProductGalleries.AsNoTracking().FirstOrDefault(o => o.Id == model.Id);
                Mapper.CreateMap<ProductGalleryModel, ProductGallery>();
                galleryImage = Mapper.Map<ProductGalleryModel, ProductGallery>(model);
                if (galleryImage != null && galleryImage.Id > 0)
                {
                    entities.Entry(galleryImage).State = System.Data.Entity.EntityState.Modified;
                }
                else
                {
                    entities.Entry(galleryImage).State = System.Data.Entity.EntityState.Added;
                }
                try
                {
                    entities.SaveChanges();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return RedirectPermanent("/admin/product/gallery/" + model.ProductId);
        }
        [HttpPost]
        public ActionResult DeleteImage(int id = 0)
        {
            int productId = 0;
            if (id > 0)
            {
                var e = entities.ProductGalleries.FirstOrDefault(o => o.Id == id);
                productId = e.ProductId;
                entities.ProductGalleries.Remove(e);
                entities.SaveChanges();

            }
            if (productId > 0)
            {
                return RedirectPermanent("/admin/product/gallery/" + productId);
            }
            else
            {
                return RedirectPermanent("/admin/product/index");
            }
        }
        #endregion
    }
}