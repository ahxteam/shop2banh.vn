﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectManagement.Areas.Admin.Controllers
{
    public class SubscribeController : Controller
    {
        private ProjectManagementEntities entities = new ProjectManagementEntities();
        // GET: Admin/Subscribe
        public ActionResult Index()
        {
            ViewBag.Subscribers = entities.Subscribes.OrderByDescending(o=>o.Id).ToList();
            return View();
        }
    }
}