﻿using AutoMapper;
using Model;
using ProjectManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using ProjectManagement.Helper;
using System.IO;

namespace ProjectManagement.Areas.Admin.Controllers
{
    public class GalleryController : Controller
    {
        private ProjectManagementEntities entities = new ProjectManagementEntities();

        public ActionResult Index()
        {
            var data = entities.Galleries.OrderByDescending(o => o.Id).ToList();
            ViewBag.Galleries = data.Select(o => new GalleryModel()
            {
                Active = o.Active,
                Description = o.Description,
                Id = o.Id,
                Name = o.Name,
                Type = o.Type,
                TypeName = ((EnumTypeGallery)o.Type).GetDescription()
            }).ToList();

            return View();
        }

        public ActionResult GalleryImages(int id)
        {
            var data = entities.GalleryImages.Where(o => o.GalleryId == id).OrderByDescending(o => o.Id).ToList();
            ViewBag.GalleryImages = data;
            return View(new GalleryImageModel() { GalleryId = id });
        }

        #region CRUD
        [Authorize]
        [HttpGet]
        public ActionResult CreateOrEdit(int id = 0)
        {
            var model = new GalleryModel()
            {
                Id = id,
                Name = string.Empty,
                TypeName = string.Empty,
                Description = string.Empty
            };
            if (id > 0)
            {
                var gallery = entities.Galleries.FirstOrDefault(o => o.Id == id);
                Mapper.CreateMap<Gallery, GalleryModel>();
                model = Mapper.Map<Gallery, GalleryModel>(gallery);
            }
            return PartialView(model);
        }

        [Authorize]
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CreateOrEdit(GalleryModel model)
        {
            var gallery = entities.Galleries.AsNoTracking().FirstOrDefault(o => o.Id == model.Id);
            Mapper.CreateMap<GalleryModel, Gallery>();
            gallery = Mapper.Map<GalleryModel, Gallery>(model);
            if (gallery != null && gallery.Id > 0)
            {
                entities.Entry(gallery).State = System.Data.Entity.EntityState.Modified;
            }
            else
            {
                entities.Entry(gallery).State = System.Data.Entity.EntityState.Added;
            }
            try
            {
                entities.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Json(new { result = true });
        }
        [HttpPost]
        public ActionResult Delete(int id = 0)
        {
            if (id > 0)
            {
                entities.Galleries.Remove(entities.Galleries.FirstOrDefault(o => o.Id == id));
                entities.SaveChanges();
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            return Json(false, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public ActionResult GalleryImages(GalleryImageModel model, HttpPostedFileBase FileBase)
        {
            if (!ModelState.IsValid)
            {
                return Json(new
                {
                    result = false,
                    Errors =
                    ModelState.Keys.Where(
                        p => ModelState[p].Errors.Any(e => string.IsNullOrEmpty(e.ErrorMessage) == false)).
                        Select(p => new
                        {
                            field = p,
                            msgs = ModelState[p].Errors.Select(e => e.ErrorMessage)
                        })
                });
            }
            else
            {
                if (FileBase != null && FileBase.ContentLength > 0)
                {
                    var fileName = Path.GetFileName(FileBase.FileName);
                    fileName = string.Format("{0:yyyyMMddhhmmss}_{1}", DateTime.Now, fileName);
                    var path = Path.Combine(Server.MapPath("~/Uploads/Images"), fileName);
                    FileBase.SaveAs(path);
                    model.Image = Path.Combine("/Uploads/Images", fileName);
                }
                else
                {
                    model.Image = "";
                }
                var galleryImage = entities.GalleryImages.AsNoTracking().FirstOrDefault(o => o.Id == model.Id);
                Mapper.CreateMap<GalleryImageModel, GalleryImage>();
                galleryImage = Mapper.Map<GalleryImageModel, GalleryImage>(model);
                if (galleryImage != null && galleryImage.Id > 0)
                {
                    entities.Entry(galleryImage).State = System.Data.Entity.EntityState.Modified;
                }
                else
                {
                    entities.Entry(galleryImage).State = System.Data.Entity.EntityState.Added;
                }
                try
                {
                    entities.SaveChanges();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return RedirectPermanent("/admin/gallery/galleryimages/" + model.GalleryId);
        }
        [HttpPost]
        public ActionResult DeleteImage(int id = 0)
        {
            if (id > 0)
            {
                entities.GalleryImages.Remove(entities.GalleryImages.FirstOrDefault(o => o.Id == id));
                entities.SaveChanges();
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            return Json(false, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}