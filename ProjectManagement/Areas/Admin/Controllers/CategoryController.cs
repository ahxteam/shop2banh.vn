﻿using AutoMapper;
using Model;
using ProjectManagement.Helper;
using ProjectManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
namespace ProjectManagement.Areas.Admin.Controllers
{
    public class CategoryController : Controller
    {
        private ProjectManagementEntities entities = new ProjectManagementEntities();

        public ActionResult Index()
        {
            ViewBag.Categories = entities.Categories.Where(o => o.Parent == 0).OrderByDescending(o => o.OrderDisplay).ToList();
            return View();
        }
        public ActionResult SubCategory(int id)
        {
            ViewBag.Categories = entities.Categories.Where(o => o.Parent == id).OrderByDescending(o => o.OrderDisplay).ToList();
            return View();
        }
        #region CRUD
        [Authorize]
        [HttpGet]
        public ActionResult CreateOrEdit(int id = 0)
        {
            var model = new CategoryModel()
            {
                Id = 0,
                Description = string.Empty,
                UrlCode = string.Empty,
                Name = string.Empty,
                OrderDisplay = 0,
                Parent = id
            };
            if (id > 0)
            {
                var entity = entities.Categories.FirstOrDefault(o => o.Id == id);
                Mapper.CreateMap<Category, CategoryModel>();
                model = Mapper.Map<Category, CategoryModel>(entity);
            }
            ViewBag.Categories = entities.Categories.Where(o => o.Id != id).ToList();
            return PartialView(model);
        }

        [Authorize]
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CreateOrEdit(CategoryModel model)
        {
            if (!ModelState.IsValid)
            {
                return Json(new
                {
                    result = false,
                    Errors =
                    ModelState.Keys.Where(
                        p => ModelState[p].Errors.Any(e => string.IsNullOrEmpty(e.ErrorMessage) == false)).
                        Select(p => new
                        {
                            field = p,
                            msgs = ModelState[p].Errors.Select(e => e.ErrorMessage)
                        })
                });
            }
            else
            {
                model.CreatedDate = DateTime.Now;
                model.UpdatedDate = DateTime.Now;
                model.UserId = User.Identity.GetUserId();
                if (string.IsNullOrEmpty(model.UrlCode))
                {
                    model.UrlCode = UtilHelper.RejectMarks(model.Name);
                }
                else
                {
                    model.UrlCode = UtilHelper.RejectMarks(model.UrlCode);
                }
                if (model.OrderDisplay <= 0)
                {
                    model.OrderDisplay = entities.Categories.Max(o => o.OrderDisplay) + 1;
                }
                var cate = entities.Categories.AsNoTracking().FirstOrDefault(o => o.Id == model.Id);
                var subCategory = entities.Categories.AsNoTracking().Where(o => o.Parent == o.Id).ToList();
                subCategory = subCategory.Select(s =>
                {
                    s.Active = model.Active;
                    return s;
                }).ToList();

                Mapper.CreateMap<CategoryModel, Category>();
                cate = Mapper.Map<CategoryModel, Category>(model);
                if (cate != null && cate.Id > 0)
                {
                    entities.Entry(cate).State = System.Data.Entity.EntityState.Modified;
                }
                else
                {
                    entities.Entry(cate).State = System.Data.Entity.EntityState.Added;
                }
                try
                {
                    entities.SaveChanges();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                return Json(new { result = true });
            }
        }
        [HttpPost]
        public ActionResult Delete(int id = 0)
        {
            if (id > 0)
            {
                var entity = entities.Categories.FirstOrDefault(o => o.Id == id);
                entities.Categories.RemoveRange(entities.Categories.Where(o => o.Parent == entity.Id));
                entities.Categories.Remove(entity);
                entities.SaveChanges();
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            return Json(false, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}