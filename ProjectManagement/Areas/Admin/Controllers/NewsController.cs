﻿using AutoMapper;
using Model;
using PagedList;
using ProjectManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using ProjectManagement.Helper;
using System.IO;

namespace ProjectManagement.Areas.Admin.Controllers
{
    public class NewsController : Controller
    {
        private ProjectManagementEntities entities = new ProjectManagementEntities();

        public ViewResult Index(string sortOrder, string currentFilter, string searchString, int? page)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.DateSortParm = sortOrder == "Date" ? "date_desc" : "Date";

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;

            var news = entities.News.ToList();
            if (!String.IsNullOrEmpty(searchString))
            {
                news = news.Where(s => s.Title.Contains(searchString)
                                       || s.Title.Contains(searchString)).ToList();
            }
            switch (sortOrder)
            {
                case "title_desc":
                    news = news.OrderByDescending(s => s.Title).ToList();
                    break;
                case "created_desc":
                    news = news.OrderBy(s => s.CreatedDate).ToList();
                    break;
                case "view_desc":
                    news = news.OrderByDescending(s => s.View).ToList();
                    break;
                default:
                    news = news.OrderByDescending(s => s.CreatedDate).ToList();
                    break;
            }

            int pageSize = 25;
            int pageNumber = (page ?? 1);

            //Mapper.CreateMap<List<News>, List<NewsModel>>();
            //var newsModelList = Mapper.Map<List<News>, List<NewsModel>>(news);
            var newsModelList = news.Select(o => new NewsModel()
            {
                Active = o.Active,
                Content = o.Content,
                CreatedDate = o.CreatedDate,
                Description = o.Description,
                Keyword = o.Keyword,
                MenuID = o.MenuID,
                NewsID = o.NewsID,
                MenuName = entities.Menus.FirstOrDefault(s => s.MenuId == o.MenuID)?.Name,
                Summary = o.Summary,
                OrderDisplay = o.OrderDisplay,
                Tags = o.Tags,
                UrlCode = o.UrlCode,
                ThumbPic = o.ThumbPic,
                Title = o.Title,
                UpdatedDate = o.UpdatedDate,
                View = o.View
            }).ToList();

            // for seo
            return View(newsModelList.ToPagedList(pageNumber, pageSize));
        }

        #region CRUD
        [Authorize]
        [HttpGet]
        public ActionResult CreateOrEdit(int id = 0)
        {
            ViewBag.Menus = entities.Menus.ToList();
            var model = new NewsModel()
            {
                NewsID = id
            };
            if (id > 0)
            {
                var news = entities.News.Where(o => o.NewsID == id).ToList();
                Mapper.CreateMap<News, NewsModel>();
                model = Mapper.Map<News, NewsModel>(news.FirstOrDefault());
            }
            return View(model);
        }

        [Authorize]
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CreateOrEdit(NewsModel model)
        {
            ViewBag.Menus = entities.Menus.ToList();
            model.CreatedDate = DateTime.Now;
            model.UpdatedDate = DateTime.Now;
            model.UserID = User.Identity.GetUserId();
            model.UpdatedUserID = User.Identity.GetUserId();
            model.View = 0;
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            else
            {
                if (model.OrderDisplay <= 0)
                {
                    model.OrderDisplay = entities.News.Max(o => o.OrderDisplay) + 1;
                }
                if (model.FileBase != null && model.FileBase.ContentLength > 0)
                {
                    var fileName = Path.GetFileName(model.FileBase.FileName);
                    var path = Path.Combine(Server.MapPath("~/Uploads/Images"), fileName);
                    model.FileBase.SaveAs(path);
                    model.ThumbPic = Path.Combine("/Uploads/Images", fileName);
                }

                if (string.IsNullOrEmpty(model.UrlCode))
                {
                    model.UrlCode = UtilHelper.RejectMarks(model.Title);
                }
                else
                {
                    model.UrlCode = UtilHelper.RejectMarks(model.UrlCode);
                }

                if (string.IsNullOrEmpty(model.Description))
                {
                    model.Description = model.Title;

                }

                var news = entities.News.AsNoTracking().FirstOrDefault(o => o.NewsID == model.NewsID);
                Mapper.CreateMap<NewsModel, News>();
                news = Mapper.Map<NewsModel, News>(model);
                if (news != null && news.NewsID > 0)
                {
                    entities.Entry(news).State = System.Data.Entity.EntityState.Modified;
                }
                else
                {
                    entities.Entry(news).State = System.Data.Entity.EntityState.Added;
                }
                try
                {
                    entities.SaveChanges();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                return RedirectPermanent("/admin/news/index");
            }

        }

        [HttpPost]
        public ActionResult Delete(int id = 0)
        {
            if (id > 0)
            {
                entities.News.Remove(entities.News.FirstOrDefault(o => o.NewsID == id));
                entities.SaveChanges();
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            return Json(false, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}
