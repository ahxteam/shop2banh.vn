﻿using AutoMapper;
using Model;
using ProjectManagement.Helper;
using ProjectManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectManagement.Areas.Admin.Controllers
{
    public class PagesController : Controller
    {
        private ProjectManagementEntities entities = new ProjectManagementEntities();

        public ActionResult Index()
        {
            ViewBag.Pages = entities.Pages.OrderByDescending(o => o.OrderDisplay).ToList();
            return View();
        }

        #region CRUD
        [Authorize]
        [HttpGet]
        public ActionResult CreateOrEdit(int id = 0)
        {
            var model = new PageModel()
            {
                Id = 0,
                Description = string.Empty,
                UrlCode = string.Empty,
                Name = string.Empty,
                OrderDisplay = 0,
                Content = string.Empty
            };
            if (id > 0)
            {
                var page = entities.Pages.FirstOrDefault(o => o.Id == id);
                Mapper.CreateMap<Page, PageModel>();
                model = Mapper.Map<Page, PageModel>(page);
            }

            var template = new Dictionary<int, string>();
            template.Add((int)EnumTemplateType.TemplateInterface, EnumTemplateType.TemplateInterface.GetDescription());
            template.Add((int)EnumTemplateType.TemplateProgress, EnumTemplateType.TemplateProgress.GetDescription());
            template.Add((int)EnumTemplateType.TemplateRating, EnumTemplateType.TemplateRating.GetDescription());
            template.Add((int)EnumTemplateType.TemplateStatistic, EnumTemplateType.TemplateStatistic.GetDescription());
            template.Add((int)EnumTemplateType.TemplateUsefulPosition, EnumTemplateType.TemplateUsefulPosition.GetDescription());
            ViewBag.TemplateTypes = template;

            return View(model);
        }

        [Authorize]
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CreateOrEdit(PageModel model)
        {
            model.CreatedDate = DateTime.Now;
            if (ModelState.IsValid)
            {
                model.CreatedDate = DateTime.Now;
                if (string.IsNullOrEmpty(model.UrlCode))
                {
                    model.UrlCode = UtilHelper.RejectMarks(model.Name);
                }
                else
                {
                    model.UrlCode = UtilHelper.RejectMarks(model.UrlCode);
                }
                if (model.OrderDisplay <= 0)
                {
                    model.OrderDisplay = entities.Pages.Max(o => o.OrderDisplay) + 1;
                }
                if (string.IsNullOrEmpty(model.Description))
                {
                    model.Description = model.Name;
                }
                if (string.IsNullOrEmpty(model.Keyword))
                {
                    model.Keyword = model.Name;
                }
                var page = entities.Pages.AsNoTracking().FirstOrDefault(o => o.Id == model.Id);
                Mapper.CreateMap<PageModel, Page>();
                page = Mapper.Map<PageModel, Page>(model);
                if (page != null && page.Id > 0)
                {
                    entities.Entry(page).State = System.Data.Entity.EntityState.Modified;
                }
                else
                {
                    entities.Entry(page).State = System.Data.Entity.EntityState.Added;
                }
                try
                {
                    entities.SaveChanges();
                    return Redirect("/admin/pages/index");
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            var template = new Dictionary<int, string>();
            template.Add((int)EnumTemplateType.TemplateInterface, EnumTemplateType.TemplateInterface.GetDescription());
            template.Add((int)EnumTemplateType.TemplateProgress, EnumTemplateType.TemplateProgress.GetDescription());
            template.Add((int)EnumTemplateType.TemplateRating, EnumTemplateType.TemplateRating.GetDescription());
            template.Add((int)EnumTemplateType.TemplateStatistic, EnumTemplateType.TemplateStatistic.GetDescription());
            template.Add((int)EnumTemplateType.TemplateUsefulPosition, EnumTemplateType.TemplateUsefulPosition.GetDescription());
            ViewBag.TemplateTypes = template;
            return View(model);
        }
        [HttpPost]
        public ActionResult Delete(int id = 0)
        {
            if (id > 0)
            {
                var page = entities.Pages.FirstOrDefault(o => o.Id == id);
                entities.Pages.Remove(page);
                entities.SaveChanges();
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            return Json(false, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}