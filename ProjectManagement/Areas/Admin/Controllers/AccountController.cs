﻿using System;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using ProjectManagement.Models;
using Model;
using AutoMapper;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Web.Security;
using System.IO;
using System.Net;
using System.Collections.Generic;
using System.Data.Entity;

namespace ProjectManagement.Areas.Admin.Controllers
{
    //[Authorize]
    public class AccountController : Controller
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;
        private ProjectManagementEntities entities = new ProjectManagementEntities();
        public AccountController()
        {
        }

        public AccountController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            // This doesn't count login failures towards account lockout
            // To enable password failures to trigger account lockout, change to shouldLockout: true
            var result = await SignInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, shouldLockout: false);
            switch (result)
            {
                case SignInStatus.Success:
                    //var userPic = entities.Profiles.FirstOrDefault(o => o.UserId.Equals(User.Identity.GetUserId())).ProfilePicture;
                    //Session["ProfilePicture"] = userPic;
                    return RedirectToLocal(returnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = model.RememberMe });
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Tên đăng nhập hoặc mật khẩu không chính xác.");
                    return View(model);
            }
        }

        //
        // GET: /Account/VerifyCode
        [AllowAnonymous]
        public async Task<ActionResult> VerifyCode(string provider, string returnUrl, bool rememberMe)
        {
            // Require that the user has already logged in via username/password or external login
            if (!await SignInManager.HasBeenVerifiedAsync())
            {
                return View("Error");
            }
            return View(new VerifyCodeViewModel { Provider = provider, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        public async Task<ActionResult> Delete(string id)
        {
            var context = new ApplicationDbContext();
            if (ModelState.IsValid)
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

                var user = await UserManager.FindByIdAsync(id);
                var logins = user.Logins;
                var rolesForUser = await UserManager.GetRolesAsync(id);

                using (var transaction = context.Database.BeginTransaction())
                {
                    foreach (var login in logins.ToList())
                    {
                        await UserManager.RemoveLoginAsync(login.UserId, new UserLoginInfo(login.LoginProvider, login.ProviderKey));
                    }

                    if (rolesForUser.Count() > 0)
                    {
                        foreach (var item in rolesForUser.ToList())
                        {
                            // item should be the name of the role
                            var result = await UserManager.RemoveFromRoleAsync(user.Id, item);
                        }
                    }

                    await UserManager.DeleteAsync(user);
                    transaction.Commit();
                }

                return RedirectToAction("Index");
            }
            else
            {
                return View();
            }
        }
        //
        // POST: /Account/VerifyCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VerifyCode(VerifyCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            // The following code protects for brute force attacks against the two factor codes. 
            // If a user enters incorrect codes for a specified amount of time then the user account 
            // will be locked out for a specified amount of time. 
            // You can configure the account lockout settings in IdentityConfig
            var result = await SignInManager.TwoFactorSignInAsync(model.Provider, model.Code, isPersistent: model.RememberMe, rememberBrowser: model.RememberBrowser);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(model.ReturnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Invalid code.");
                    return View(model);
            }
        }

        //
        // GET: /Account/Register
        //[AllowAnonymous]
        //[Authorize(Roles = "Admin")]
        public ActionResult Register(string userId)
        {
            var context = new ApplicationDbContext();
            ViewBag.Roles = context.Roles.ToList();
            return PartialView(new RegisterViewModel());



        }

        public ActionResult UpdateProfile(string id)
        {
            var context = new ApplicationDbContext();
            ViewBag.Roles = context.Roles.ToList();
            var user = context.Users.FirstOrDefault(o => o.Id.Equals(id));
            return PartialView(new ProfileModel()
            {
                Address = user.Address,
                Email = user.Email,
                ProfilePicture = user.ProfilePicture,
                Phone = user.PhoneNumber,
                RoleId = user.Roles.FirstOrDefault().RoleId,
                UserId = user.Id,
                Name = user.Name
            });

        }
        // GET: /Account/PersonalInfomation
        [AllowAnonymous]
        public ActionResult PersonalInformation()
        {
            ProfileModel personalInfor = new ProfileModel();
            var userID = User.Identity.GetUserId();

            //Mapper.CreateMap<Model.Profile, ProfileModel>();

            //var profile = entities.Profiles.FirstOrDefault(o => o.UserId == userID);
            //personalInfor = Mapper.Map<Model.Profile, ProfileModel>(profile);
            //ViewBag.Positions = entities.Positions.ToList();
            //return PartialView(personalInfor);
            return View();
        }

        //
        // POST: /Account/PersonalInformation
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult PersonalInformation(ProfileModel model, HttpPostedFileBase FileBase)
        {
            if (ModelState.IsValid)
            {
                if (FileBase != null && FileBase.ContentLength > 0)
                {
                    var fileName = Path.GetFileName(FileBase.FileName);
                    var path = Path.Combine(Server.MapPath("~/Content/Images"), fileName);
                    FileBase.SaveAs(path);
                    model.ProfilePicture = Path.Combine("/Content/Images", fileName);
                }
                else
                {
                    model.ProfilePicture = "";
                }
                // add profile
                //Mapper.CreateMap<ProfileModel, Model.Profile>();
                //var profile = entities.Profiles.FirstOrDefault(o => o.UserId == model.UserId);
                //profile.Name = model.Name;
                //profile.Address = model.Address;
                //profile.Phone = model.Phone;
                //profile.PositionId = model.PositionId;
                //profile.ProfilePicture = model.ProfilePicture;
                //entities.SaveChanges();

                return RedirectToAction("Index", "Dashboard");
            }
            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        //[ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterViewModel model, HttpPostedFileBase FileBase)
        {
            var context = new ApplicationDbContext();
            ViewBag.Roles = context.Roles.ToList();
            if (!ModelState.IsValid)
            {
                return Json(new
                {
                    result = false,
                    Errors =
                        ModelState.Keys.Where(
                            p => ModelState[p].Errors.Any(e => string.IsNullOrEmpty(e.ErrorMessage) == false)).
                            Select(p => new
                            {
                                field = p,
                                msgs = ModelState[p].Errors.Select(e => e.ErrorMessage)
                            })
                });
            }
            else
            {
                if (FileBase != null && FileBase.ContentLength > 0)
                {
                    var fileName = $"{DateTime.Now.ToString()}{Path.GetExtension(model.FileBase.FileName)}";
                    var path = Path.Combine(Server.MapPath("~/Uploads/Images"), fileName);
                    model.FileBase.SaveAs(path);
                    model.ProfilePicture = Path.Combine("/Uploads/Images", fileName);
                }
                var user = new ApplicationUser
                {
                    UserName = model.Email,
                    Email = model.Email,
                    Name = model.Name,
                    Address = model.Address,
                    ProfilePicture = model.ProfilePicture,
                    PhoneNumber = model.Phone
                };
                var result = await UserManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    //await SignInManager.SignInAsync(user, isPersistent:false, rememberBrowser:false);
                    // get role
                    var roleId = model.RoleId;
                    var role = entities.AspNetRoles.FirstOrDefault(r => r.Id.Equals(roleId)).Name;
                    model.RoleId = roleId;
                    await UserManager.AddToRoleAsync(user.Id, role);


                    // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                    // Send an email with this link
                    string code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
                    var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                    await UserManager.SendEmailAsync(user.Id, "Xác nhận tài khoản thành viên", "Bạn vui lòng click <a href=\"" + callbackUrl + "\">tại đây</a> để xác nhận email.");

                    return Json(new { result = true });
                }
                else
                {
                    var listErrors = new List<dynamic>();
                    foreach (var item in result.Errors)
                    {
                        listErrors.Add(new
                        {
                            field = "UserId",
                            msgs = new List<string>() { item.Contains("Password") ? "Lỗi: Mật khẩu phải có 1 ký tự hoa, 1 ký tự đặc biệt và số. VD: Admin123@" : "Email hoặc tên đã được sử dụng" }
                        });
                    }
                    return Json(new
                    {
                        result = false,
                        Errors = listErrors
                    });
                }
                //AddErrors(result);
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        //[ValidateAntiForgeryToken]
        public async Task<ActionResult> UpdateProfile(ProfileModel model, HttpPostedFileBase FileBase)
        {
            var context = new ApplicationDbContext();
            ViewBag.Roles = context.Roles.ToList();
            if (!ModelState.IsValid)
            {
                return Json(new
                {
                    result = false,
                    Errors =
                        ModelState.Keys.Where(
                            p => ModelState[p].Errors.Any(e => string.IsNullOrEmpty(e.ErrorMessage) == false)).
                            Select(p => new
                            {
                                field = p,
                                msgs = ModelState[p].Errors.Select(e => e.ErrorMessage)
                            })
                });
            }
            else
            {
                //var store = new UserStore<ApplicationUser>(context);
                //var manager = new UserManager(store,);
                //var user = context.Users.AsNoTracking().FirstOrDefault(o => o.Id.Equals(model.UserId));
                var user = UserManager.FindById(model.UserId);
                if (FileBase != null && FileBase.ContentLength > 0)
                {
                    var fileName = $"{user.Id}{Path.GetExtension(model.FileBase.FileName)}";
                    var path = Path.Combine(Server.MapPath("~/Uploads/Images"), fileName);
                    model.FileBase.SaveAs(path);
                    model.ProfilePicture = Path.Combine("/Uploads/Images", fileName);
                }
                user.ProfilePicture = model.ProfilePicture;
                user.Name = model.Name;
                user.Address = model.Address;
                user.PhoneNumber = model.Phone;
                //context.Entry(user).State = EntityState.Modified;
                var result = await UserManager.UpdateAsync(user);
                if (result.Succeeded)
                {
                    //var roleId = user.Roles.FirstOrDefault().RoleId;
                    var role = entities.AspNetRoles.FirstOrDefault(r => r.Id.Equals(model.RoleId)).Name;
                    //model.RoleId = roleId;
                    await UserManager.RemoveFromRoleAsync(model.UserId, model.RoleId);
                    await UserManager.AddToRoleAsync(model.UserId, role);

                    return Redirect("/admin/account");
                }
                else
                {
                    var listErrors = new List<dynamic>();
                    foreach (var item in result.Errors)
                    {
                        listErrors.Add(new
                        {
                            field = "",
                            msgs = new List<string>() { item.Contains("Password") ? "Lỗi: Mật khẩu phải có 1 ký tự hoa, 1 ký tự đặc biệt và số. VD: Admin123@" : "Email hoặc tên đã được sử dụng" }
                        });
                    }
                    return Json(new
                    {
                        result = false,
                        Errors = listErrors
                    });
                }
                //AddErrors(result);
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }
        //[Authorize(Roles = "Admin")]
        public ActionResult Index()
        {
            var context = new ApplicationDbContext();
            var list = context.Users.ToList();
            var listProfile = new List<RegisterViewModel>();
            for (int i = 0; i < list.Count; i++)
            {
                var p = new RegisterViewModel
                {
                    Name = list[i].Name,
                    Phone = list[i].PhoneNumber,
                    Email = list[i].Email,
                    UserId = list[i].Id
                    //RoleName = list[i].Roles.Count > 0 ? _userManager.GetRoles(list[i].Id).FirstOrDefault() : string.Empty
                };
                if (list[i].Roles.Count > 0)
                {
                    //var role = context.Roles.Where(o => o.Id.Equals(list[i].Roles.FirstOrDefault().RoleId)).FirstOrDefault();
                    //p.RoleName = role.Name;
                }
                listProfile.Add(p);
            }
            ViewBag.Profiles = listProfile;
            return View();
        }
        [Authorize]
        [Authorize(Roles = "Admin")]
        public ActionResult CreateOrEdit(int id = 0)
        {
            var model = new ProfileModel();
            if (id > 0)
            {
                //var profile = entities.Profiles.Where(o => o.Id == id).FirstOrDefault();
                //Mapper.CreateMap<Model.Profile, ProfileModel>();
                //model = Mapper.Map<Model.Profile, ProfileModel>(profile);
            }
            return PartialView("CreateOrEdit");
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        public ActionResult CreateOrEdit(ProfileModel model)
        {
            //var profile = entities.Profiles.FirstOrDefault(o => o.Id == model.Id && o.Id > 0);
            //if (profile != null)
            //{
            //    entities.Entry(profile).State = System.Data.Entity.EntityState.Modified;
            //}
            //else
            //{
            //    profile = new Model.Profile
            //    {
            //        Name = model.Name,
            //        Email = model.Email,
            //        Address = model.Address,
            //        PositionId = model.PositionId,
            //        Phone = "09899999999",
            //        LastLogin = DateTime.Now,
            //        Position = entities.Positions.FirstOrDefault(p => p.Id == model.PositionId).Name ?? ""
            //    };
            //    entities.Entry(profile).State = System.Data.Entity.EntityState.Added;
            //    entities.Profiles.Add(profile);
            //}
            //Mapper.CreateMap<ProfileModel, Model.Profile>();
            //profile = Mapper.Map<ProfileModel, Model.Profile>(model);
            //entities.SaveChanges();
            return Redirect("Index");
        }
        //
        // GET: /Account/ConfirmEmail
        [AllowAnonymous]
        public async Task<ActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return View("Error");
            }
            var result = await UserManager.ConfirmEmailAsync(userId, code);
            return View(result.Succeeded ? "ConfirmEmail" : "Error");
        }

        //
        // GET: /Account/ForgotPassword
        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        //
        // POST: /Account/ForgotPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await UserManager.FindByNameAsync(model.Email);
                if (user == null || !(await UserManager.IsEmailConfirmedAsync(user.Id)))
                {
                    // Don't reveal that the user does not exist or is not confirmed
                    string code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
                    var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                    await UserManager.SendEmailAsync(user.Id, "Reset mật khẩu", "Để khôi phục mật khẩu bạn vui lòng click <a href=\"" + callbackUrl + "\">tại đây</a>");
                    return RedirectToAction("ForgotPasswordConfirmation", "Account", model);
                }

                // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                // Send an email with this link
                //string code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
                //var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                //await UserManager.SendEmailAsync(user.Id, "Reset Password", "Please reset your password by clicking <a href=\"" + callbackUrl + "\">here</a>");
                //return RedirectToAction("ForgotPasswordConfirmation", "Account");
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/ForgotPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        //
        // GET: /Account/ResetPassword
        [AllowAnonymous]
        public ActionResult ResetPassword(string code)
        {
            return code == null ? View("Error") : View();
        }

        //
        // POST: /Account/ResetPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var user = await UserManager.FindByNameAsync(model.Email);
            if (user == null)
            {
                // Don't reveal that the user does not exist
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            var result = await UserManager.ResetPasswordAsync(user.Id, model.Code, model.Password);
            if (result.Succeeded)
            {
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            AddErrors(result);
            return View();
        }

        //
        // GET: /Account/ResetPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        //
        // POST: /Account/ExternalLogin
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            // Request a redirect to the external login provider
            return new ChallengeResult(provider, Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl }));
        }

        //
        // GET: /Account/SendCode
        [AllowAnonymous]
        public async Task<ActionResult> SendCode(string returnUrl, bool rememberMe)
        {
            var userId = await SignInManager.GetVerifiedUserIdAsync();
            if (userId == null)
            {
                return View("Error");
            }
            var userFactors = await UserManager.GetValidTwoFactorProvidersAsync(userId);
            var factorOptions = userFactors.Select(purpose => new SelectListItem { Text = purpose, Value = purpose }).ToList();
            return View(new SendCodeViewModel { Providers = factorOptions, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        //
        // POST: /Account/SendCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SendCode(SendCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            // Generate the token and send it
            if (!await SignInManager.SendTwoFactorCodeAsync(model.SelectedProvider))
            {
                return View("Error");
            }
            return RedirectToAction("VerifyCode", new { Provider = model.SelectedProvider, ReturnUrl = model.ReturnUrl, RememberMe = model.RememberMe });
        }

        //
        // GET: /Account/ExternalLoginCallback
        [AllowAnonymous]
        public async Task<ActionResult> ExternalLoginCallback(string returnUrl)
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync();
            if (loginInfo == null)
            {
                return RedirectToAction("Login");
            }

            // Sign in the user with this external login provider if the user already has a login
            var result = await SignInManager.ExternalSignInAsync(loginInfo, isPersistent: false);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(returnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = false });
                case SignInStatus.Failure:
                default:
                    // If the user does not have an account, then prompt the user to create an account
                    ViewBag.ReturnUrl = returnUrl;
                    ViewBag.LoginProvider = loginInfo.Login.LoginProvider;
                    return View("ExternalLoginConfirmation", new ExternalLoginConfirmationViewModel { Email = loginInfo.Email });
            }
        }

        //
        // POST: /Account/ExternalLoginConfirmation
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ExternalLoginConfirmation(ExternalLoginConfirmationViewModel model, string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Manage");
            }

            if (ModelState.IsValid)
            {
                // Get the information about the user from the external login provider
                var info = await AuthenticationManager.GetExternalLoginInfoAsync();
                if (info == null)
                {
                    return View("ExternalLoginFailure");
                }
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
                var result = await UserManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    result = await UserManager.AddLoginAsync(user.Id, info.Login);
                    if (result.Succeeded)
                    {
                        await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                        return RedirectToLocal(returnUrl);
                    }
                }
                AddErrors(result);
            }

            ViewBag.ReturnUrl = returnUrl;
            return View(model);
        }

        //
        // POST: /Account/LogOff
        [HttpGet]
        //[ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return RedirectToAction("Index", "Dashboard");
        }

        //
        // GET: /Account/ExternalLoginFailure
        [AllowAnonymous]
        public ActionResult ExternalLoginFailure()
        {
            return View();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_userManager != null)
                {
                    _userManager.Dispose();
                    _userManager = null;
                }

                if (_signInManager != null)
                {
                    _signInManager.Dispose();
                    _signInManager = null;
                }
            }

            base.Dispose(disposing);
        }

        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Dashboard");
        }

        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }
        #endregion
    }
}