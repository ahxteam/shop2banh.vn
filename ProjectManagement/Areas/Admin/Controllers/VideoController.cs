﻿using AutoMapper;
using Model;
using ProjectManagement.Helper;
using ProjectManagement.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectManagement.Areas.Admin.Controllers
{
    public class VideoController : Controller
    {
        private ProjectManagementEntities entities = new ProjectManagementEntities();

        public ActionResult Index()
        {
            ViewBag.Videos = entities.Videos.OrderByDescending(o => o.CreatedDate).ToList();
            return View();
        }

        #region CRUD
        [Authorize]
        [HttpGet]
        public ActionResult CreateOrEdit(int id = 0)
        {
            var model = new VideoModel()
            {
                Id = 0,
                Description = string.Empty,
                UrlCode = string.Empty,
                Name = string.Empty,
                Content = string.Empty
            };
            if (id > 0)
            {
                var video = entities.Videos.FirstOrDefault(o => o.Id == id);
                Mapper.CreateMap<Video, VideoModel>();
                model = Mapper.Map<Video, VideoModel>(video);
            }

            var template = new Dictionary<int, string>();
            ViewBag.TemplateTypes = template;

            return View(model);
        }

        [Authorize]
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CreateOrEdit(VideoModel model)
        {
            model.CreatedDate = DateTime.Now;
            model.CodeEmbeb = model.Name;
            model.Summary = model.Name;
            if (ModelState.IsValid)
            {
                model.CreatedDate = DateTime.Now;
                if (string.IsNullOrEmpty(model.UrlCode))
                {
                    model.UrlCode = UtilHelper.RejectMarks(model.Name);
                }
                else
                {
                    model.UrlCode = UtilHelper.RejectMarks(model.UrlCode);
                }
                if (model.FileBase != null && model.FileBase.ContentLength > 0)
                {
                    var fileName = Path.GetFileName(model.FileBase.FileName);
                    var path = Path.Combine(Server.MapPath("~/Uploads/Images"), fileName);
                    model.FileBase.SaveAs(path);
                    model.Thumb = Path.Combine("/Uploads/Images", fileName);
                }
                else
                {
                    model.Thumb = "";
                }
                model.CodeEmbeb = model.Name;
                var video = entities.Videos.AsNoTracking().FirstOrDefault(o => o.Id == model.Id);
                Mapper.CreateMap<VideoModel, Video>();
                video = Mapper.Map<VideoModel, Video>(model);

                if (video != null && video.Id > 0)
                {
                    entities.Entry(video).State = System.Data.Entity.EntityState.Modified;
                }
                else
                {
                    entities.Entry(video).State = System.Data.Entity.EntityState.Added;
                }
                try
                {
                    entities.SaveChanges();
                    return Redirect("/admin/video/index");
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return View(model);
        }
        [HttpPost]
        public ActionResult Delete(int id = 0)
        {
            if (id > 0)
            {
                var video = entities.Videos.FirstOrDefault(o => o.Id == id);
                entities.Videos.Remove(video);
                entities.SaveChanges();
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            return Json(false, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}