﻿using ProjectManagement.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectManagement.Areas.Admin.Controllers
{
    public class FileController : Controller
    {
        // GET: File
        public ActionResult Index()
        {
            var allFiles = Directory.EnumerateFiles(Server.MapPath("~/Uploads/Images/"))
                              .Select(fn => new FileObject { FilePath = "/Uploads/Images/" + Path.GetFileName(fn), AbsolutePath = fn , FileName= Path.GetFileName(fn), CreatedDate = System.IO.File.GetLastWriteTime(fn) }).ToList();
            ViewBag.AllFiles = allFiles.OrderByDescending(o=>o.CreatedDate).ToList();
            return View();
        }
        public ActionResult Upload(HttpPostedFileBase FileBase)
        {
            string path;
            string saveloc = "~/Uploads/Images/";
            string filename = FileBase.FileName;

            if (FileBase != null && FileBase.ContentLength > 0 && IsImage(FileBase))
            {
                try
                {
                    path = Path.Combine(HttpContext.Server.MapPath(saveloc), Path.GetFileName(filename));
                    FileBase.SaveAs(path);
                }
                catch (Exception e)
                {
                }
            }
            return RedirectPermanent("/admin/file/index");
        }
        public string UploadTinyEMC(HttpPostedFileBase file)
        {
            string path;
            string saveloc = "~/Uploads/Images/";
            string relativeloc = "/Uploads/Images/";
            string filename = file.FileName;

            if (file != null && file.ContentLength > 0 && IsImage(file))
            {
                try
                {
                    path = Path.Combine(HttpContext.Server.MapPath(saveloc), Path.GetFileName(filename));
                    file.SaveAs(path);
                }
                catch (Exception e)
                {
                    return "<script>alert('Failed: " + e + "');</script>";
                }
            }
            else
            {
                return "<script>alert('Failed: Unkown Error. This form only accepts valid images.');</script>";
            }

            return "<script>top.$('.mce-btn.mce-open').parent().find('.mce-textbox').val('" + relativeloc + filename + "').closest('.mce-window').find('.mce-primary').click();</script>";
        }
        public bool IsImage(HttpPostedFileBase file)
        {
            if (file.ContentType.Contains("image"))
            {
                return true;
            }

            string[] formats = new string[] { ".jpg", ".png", ".gif", ".jpeg" }; // add more if u like...

            // linq from Henrik Stenbæk
            return formats.Any(item => file.FileName.EndsWith(item, StringComparison.OrdinalIgnoreCase));
        }
        [Authorize]
        public ActionResult Delete(string filePath)
        {
            if (Request.IsAuthenticated && !string.IsNullOrEmpty(filePath))
            {
                System.IO.File.Delete(Server.MapPath("~/Uploads/Images/")+filePath);
            }
            return Json(new { status = true });
        }

    }
   
}