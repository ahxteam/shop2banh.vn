﻿using AutoMapper;
using Model;
using ProjectManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;

namespace ProjectManagement.Areas.Admin.Controllers
{
    public class ResourceController : Controller
    {
        private ProjectManagementEntities entities = new ProjectManagementEntities();

        public ActionResult Index()
        {
            ViewBag.Resources = entities.Resources.OrderByDescending(o => o.Id).ToList();
            return View();
        }

        #region CRUD
        [Authorize]
        [HttpGet]
        public ActionResult CreateOrEdit(int id = 0)
        {
            var model = new ResourceModel()
            {
                Id = id,
                Name = string.Empty,
                Key = string.Empty,
                Value = string.Empty
            };
            if (id > 0)
            {
                var resource = entities.Resources.FirstOrDefault(o => o.Id == id);
                Mapper.CreateMap<Resource, ResourceModel>();
                model = Mapper.Map<Resource, ResourceModel>(resource);
            }
            return PartialView(model);
        }

        [Authorize]
        [HttpPost]
        public ActionResult CreateOrEdit(ResourceModel model)
        {
            model.CreatedDate = DateTime.Now;
            model.UpdatedDate = DateTime.Now;
            model.CreatedUser = User.Identity.GetUserId();
            model.UpdatedUser = User.Identity.GetUserId();
            var resource = entities.Resources.AsNoTracking().FirstOrDefault(o => o.Id == model.Id);
            Mapper.CreateMap<ResourceModel, Resource>();
            resource = Mapper.Map<ResourceModel, Resource>(model);
            if (resource != null && resource.Id > 0)
            {
                entities.Entry(resource).State = System.Data.Entity.EntityState.Modified;
            }
            else
            {
                entities.Entry(resource).State = System.Data.Entity.EntityState.Added;
            }
            try
            {

            entities.SaveChanges();

            }
            catch (Exception ex)
            {

                throw ex;
            }
            return Json(new { result = true });
        }
        [HttpPost]
        public ActionResult Delete(int id = 0)
        {
            if (id > 0)
            {
                entities.Resources.Remove(entities.Resources.FirstOrDefault(o => o.Id == id));
                entities.SaveChanges();
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            return Json(false, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}

