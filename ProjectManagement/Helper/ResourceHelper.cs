﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectManagement.Helper
{
    public static class ResourceHelper
    {
        private static ProjectManagementEntities entities = new ProjectManagementEntities();
        public static string GetResourceByKey(string key)
        {
            try
            {
                return entities.Resources.AsNoTracking().FirstOrDefault(o => o.Key.Equals(key)).Value;
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }
    }
}