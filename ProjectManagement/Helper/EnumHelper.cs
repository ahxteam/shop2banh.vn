﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectManagement.Helper
{
    public enum EnumTypeGallery
    {
        [Description("Slider trang chủ")]
        Slider = 1,
        [Description("Album ảnh về cty")]
        AlbumAboutCompany = 2,
        [Description("Logo ảnh")]
        LogoImage = 3,

    }
    public enum EnumSubscribeType
    {
        [Description("Đăng ký nhận tài liệu")]
        SubscribeDocument = 1,
        [Description("Đăng ký nhận thông tin")]
        SubscribeInformation = 2
    }
    public enum EnumTemplateType
    {
        [Description("Trang tĩnh")]
        TemplateStatistic = 1,
        [Description("Template vị trí tiện ích")]
        TemplateUsefulPosition = 2,
        [Description("Template tiến độ thi công")]
        TemplateProgress = 3,
        [Description("Template đánh giá")]
        TemplateRating = 4,
        [Description("Template mặt bằng căn hộ")]
        TemplateInterface = 5
    }
    public enum EnumTypeComment
    {
        [Description("Ý kiến của học viên")]
        StudentComment = 1,
        [Description("Comment về dịch vụ của cty")]
        ServiceComment = 2,

    }
    public static class EnumHelper
    {

        public static List<SelectListItem> GetEnumToList(Type type)
        {
            List<SelectListItem> lstSelectListItem = new List<SelectListItem>();
            foreach (object item in Enum.GetValues(type))
            {
                SelectListItem objSelectListItem = new SelectListItem();
                objSelectListItem.Value = ((int)item).ToString();
                objSelectListItem.Text = GetDescription((Enum)item);
                lstSelectListItem.Add(objSelectListItem);
            }

            return lstSelectListItem;
        }

        public static List<SelectListItem> GetEnumToList_OnlyText(Type type)
        {
            List<SelectListItem> lstSelectListItem = new List<SelectListItem>();

            foreach (object item in Enum.GetValues(type))
            {
                SelectListItem objSelectListItem = new SelectListItem();
                objSelectListItem.Value = GetDescription((Enum)item);
                objSelectListItem.Text = objSelectListItem.Value;
                lstSelectListItem.Add(objSelectListItem);
            }

            return lstSelectListItem;
        }

        public static string GetDescription(this Enum value)
        {
            try
            {
                var da = (DescriptionAttribute[])(value.GetType().GetField(value.ToString())).GetCustomAttributes(typeof(DescriptionAttribute), false);
                return da.Length > 0 ? da[0].Description : value.ToString();
            }
            catch
            {
                return null;
            }
        }
        public static string GetDescriptionByIntValue(this Enum value)
        {
            try
            {
                var da = (DescriptionAttribute[])(value.GetType().GetField(value.ToString())).GetCustomAttributes(typeof(DescriptionAttribute), false);
                return da.Length > 0 ? da[0].Description : value.ToString();
            }
            catch
            {
                return "N/A";
            }
        }

        public static string GetDescription(Type type, object value)
        {
            try
            {
                foreach (object item in Enum.GetValues(type))
                {
                    if (((int)item).ToString() == value.ToString())
                        return GetDescription((Enum)item);
                }

                return string.Empty;
            }
            catch
            {
                return null;
            }
        }

        public static string getEnumName(Type type, object value)
        {
            return Enum.GetName(type, value);
        }

    }
}