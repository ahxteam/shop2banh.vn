﻿var memberController = {
    currentMember: 0,
    showFormAddNew: function () {
        $.ajax({
            method: "GET",
            url: "/admin/account/register",
            async: false
        }).done(function (response) {
            $('.modal').remove();
            $("body").append(response);
            $("button[name=add-new]").click();
        });
    },
    showFormPersonalInformation: function () {
        $.ajax({
            method: "GET",
            url: "/admin/account/personalinformation",
            async: false
        }).done(function (response) {
            if ($(".modal-dialog").length > 0) {
                $(".modal-dialog").parent().remove();
            }
            $("body").append(response);
            $("button[name=add-new]").click();
        });
    },
    deleteMember: function () {
        $.ajax({
            method: "POST",
            url: "/admin/account/delete",
            data: {
                id: memberController.currentMember
            },
            async: false
        }).done(function (response) {
            window.location.reload();
        });
    }
,
    saveData: function () {
        var model = {
            Id: $("#Id").val(),
            Name: $("#Name").val(),
            Email: $("#Email").val(),
            Phone: $("#Phone").val(),
            RoleId: $("#RoleId").val(),
            Password: $("#Password").val(),
            ConfirmPassword: $("#ConfirmPassword").val(),
            Address: $("#Address").val(),
            FileBase: $("#FileBase").val(),
            ProfilePicture: $("#ProfilePicture").val(),
        }
        var vld = window.baseJs.validation;
        var $form = $("#create");
        var jsonData = JSON.stringify(model);
        $.ajax({
            url: '/admin/account/register',
            data: jsonData,
            type: 'POST',
            contentType: 'application/json',
            dataType: 'json',
            cache: false,
            success: function (response) {
                if (response.result === true) {
                    $('#con-close-modal').modal('toggle');
                    window.location.reload();
                } else if (response.Errors) {
                    vld.clear($form);
                    vld.all(response.Errors, $form);
                } else {
                    $('#con-close-modal').modal('toggle');
                }
            }
        });
    },
    showFormUpdateProfile: function () {
        debugger;
        $.ajax({
            method: "GET",
            url: "/admin/account/updateprofile",
            async: false,
            data: {
                id: memberController.currentMember
            },
        }).done(function (response) {
            $('.modal').remove();
            $("body").append(response);
            $("button[name=update-profile]").click();
        });
    },
    saveUpdateProfile: function () {
        var model = {
            Id: $("#UserId").val(),
            Name: $("#Name").val(),
            Email: $("#Email").val(),
            Phone: $("#Phone").val(),
            RoleId: $("#RoleId").val(),
            Address: $("#Address").val(),
            FileBase: $("#FileBase").val(),
            ProfilePicture: $("#ProfilePicture").val(),
        }
        var vld = window.baseJs.validation;
        var $form = $("#create");
        var jsonData = JSON.stringify(model);
        $.ajax({
            url: '/admin/account/updateprofile',
            data: jsonData,
            type: 'POST',
            contentType: 'application/json',
            dataType: 'json',
            cache: false,
            success: function (response) {
                if (response.result === true) {
                    $('#con-close-modal').modal('toggle');
                    window.location.reload();
                } else if (response.Errors) {
                    vld.clear($form);
                    vld.all(response.Errors, $form);
                } else {
                    $('#con-close-modal').modal('toggle');
                }
            }
        });
    },
    saveProfile: function () {
        var model = {
            Id: $("#Id").val(),
            Name: $("#Name").val(),
            Phone: $("#Phone").val(),
            Address: $("#Address").val(),
            FileBase: $("#FileBase").val(),
            ProfilePicture: $("#ProfilePicture").val(),
            PositionId: $("#PositionId").val(),
            Email: $("#Email").val(),
            UserId: $("#UserId").val(),
            RoleId: $("#RoleId").val(),
        }
        var vld = window.baseJs.validation;
        var $form = $("#create");
        var jsonData = JSON.stringify(model);
        $.ajax({
            url: '/admin/member/updateprofile',
            data: jsonData,
            type: 'POST',
            contentType: 'application/json',
            dataType: 'json',
            cache: false,
            success: function (response) {
                if (response.result === true) {
                    $('#con-close-modal').modal('toggle');
                    window.location.reload();
                } else if (response.Errors) {
                    vld.clear($form);
                    vld.all(response.Errors, $form);
                } else {
                    $('#con-close-modal').modal('toggle');
                }
            }
        });
    }
};