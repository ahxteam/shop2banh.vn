﻿var menuController = {
    currentMenu: 0,
    showFormAddNew: function () {
        $.ajax({
            method: "GET",
            url: "/admin/menu/createoredit",
            async: false,
            data: {
                id: menuController.currentMenu
            },
        }).done(function (response) {
            $('.modal').remove();
            $("body").append(response);
            $("button[name=add-new]").click();
        });
    },
    deleteMenu: function () {
        $.ajax({
            method: "POST",
            url: "/admin/menu/delete",
            data: {
                id: menuController.currentMenu
            },
            async: false
        }).done(function (response) {
            window.location.reload();
        });
    },
    saveData: function () {
        var model = {
            MenuId: $("#MenuId").val(),
            Name: $("#Name").val(),
            Description: $("#Description").val(),
            UrlCode: $("#UrlCode").val(),
            Parent: $("#Parent").val(),
            Active: $("input[name=Active]:checked").val()=="true" ? true : false,
            OrderDisplay: $("#OrderDisplay").val()
        }
        var vld = window.baseJs.validation;
        var $form = $("#createoredit");
        var jsonData = JSON.stringify(model);
        $.ajax({
            url: '/admin/menu/createoredit',
            data: jsonData,
            type: 'POST',
            contentType: 'application/json',
            dataType: 'json',
            cache: false,
            success: function (response) {
                if (response.result === true) {
                    $('#con-close-modal').modal('toggle');
                    window.location.reload();
                } else if (response.Errors) {
                    vld.clear($form);
                    vld.all(response.Errors, $form);
                } else {
                    $('#con-close-modal').modal('toggle');
                }
            }
        });
    },
};