﻿var galleryController = {
    currentGallery: 0,
    showFormAddNew: function () {
        $.ajax({
            method: "GET",
            url: "/admin/gallery/createoredit",
            async: false,
            data: {
                id: galleryController.currentGallery
            },
        }).done(function (response) {
            $('.modal').remove();
            $("body").append(response);
            $("button[name=add-new]").click();
        });
    },
    deleteGallery: function () {
        $.ajax({
            method: "POST",
            url: "/admin/gallery/delete",
            data: {
                id: galleryController.currentGallery
            },
            async: false
        }).done(function (response) {
            window.location.reload();
        });
    },
    saveData: function () {
        var model = {
            Id: $("#Id").val(),
            Name: $("#Name").val(),
            Description: $("#Description").val(),
            Type: $("#Type").val(),
            Active: $("input[name=Active]:checked").val() == "true" ? true : false,
        }
        var vld = window.baseJs.validation;
        var $form = $("#createoredit");
        var jsonData = JSON.stringify(model);
        $.ajax({
            url: '/admin/gallery/createoredit',
            data: jsonData,
            type: 'POST',
            contentType: 'application/json',
            dataType: 'json',
            cache: false,
            success: function (response) {
                if (response.result === true) {
                    $('#con-close-modal').modal('toggle');
                    window.location.reload();
                } else if (response.Errors) {
                    vld.clear($form);
                    vld.all(response.Errors, $form);
                } else {
                    $('#con-close-modal').modal('toggle');
                }
            }
        });
    },
};