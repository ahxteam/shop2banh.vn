﻿var productGalleryController = {
    currentProductGallery: 0,
    deleteImage: function () {
        $.ajax({
            method: "POST",
            url: "/admin/product/deleteimage",
            data: {
                id: productGalleryController.currentProductGallery
            },
            async: false
        }).done(function (response) {
            window.location.reload();
        });
    }
};