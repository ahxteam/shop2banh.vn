﻿var newsController = {
    currentNews:0,
    //showFormAddNew: function () {
    //    $.ajax({
    //        method: "GET",
    //        url: "/news/createoredit",
    //        async: false,
    //        data: {
    //            id: newsController.currentNews
    //        },
    //    }).done(function (response) {
    //        $('.modal').remove();
    //        $("body").append(response);
    //        $("button[name=add-new]").click();
    //    });
    //},
    deleteNews: function () {
        $.ajax({
            method: "POST",
            url: "/admin/news/delete",
            data: {
                id: newsController.currentNews
            },
            async: false
        }).done(function (response) {
            window.location.reload();
        });
    },
    saveData: function () {
        var model = {
            NewsId: $("#NewsId").val(),
            UrlCode: $("#UrlCode").val(),
            Title: $("#Title").val(),
            MenuID: $("#MenuID").val(),
            Keyword: $("#Keyword").val(),
            Description: $("#Description").val(),
            Summary: $("#Summary").val(),
            Active: $("input[name=Active]:checked").val()==true ? true : false,
            OrderDisplay: $("#OrderDisplay").val(),
            Content: tinymce.get('Content').getContent(),
            FileBase: $("#FileBase").val()
        }
        debugger;
        var vld = window.baseJs.validation;
        var $form = $("#createoredit");
        var jsonData = JSON.stringify(model);
        $.ajax({
            url: '/admin/news/createoredit',
            data: jsonData,
            type: 'POST',
            contentType: 'application/json',
            dataType: 'json',
            cache: false,
            success: function (response) {
                if (response.result === true) {
                    window.location.href('/news/index');
                } else if (response.Errors) {
                    vld.clear($form);
                    vld.all(response.Errors, $form);
                } 
            }
        });
    },
};