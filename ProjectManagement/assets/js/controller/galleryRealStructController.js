﻿var galleryRealStructController = {
    currentGalleryRealStruct: 0,
    deleteImage: function () {
        $.ajax({
            method: "POST",
            url: "/admin/realstructadmin/deleteimage",
            data: {
                id: galleryRealStructController.currentGalleryRealStruct
            },
            async: false
        }).done(function (response) {
            window.location.reload();
        });
    }
};