﻿var courseController = {
    currentCourse: 0,
    showFormAddNew: function () {
        $.ajax({
            method: "GET",
            url: "/admin/course/createoredit",
            async: false,
            data: {
                id: courseController.currentCourse
            },
        }).done(function (response) {
            $('.modal').remove();
            $("body").append(response);
            $("button[name=add-new]").click();
        });
    },
    deleteCourse: function () {
        $.ajax({
            method: "POST",
            url: "/admin/course/delete",
            data: {
                id: courseController.currentCourse
            },
            async: false
        }).done(function (response) {
            window.location.reload();
        });
    },
    saveData: function () {
        var model = {
            Id: $("#Id").val(),
            Name: $("#Name").val(),
            Description: $("#Description").val(),
            UrlCode: $("#UrlCode").val(),
            ParentId: $("#ParentId").val(),
            FileBase: $('#FileBase')[0].files[0],
            Active: $("input[name=Active]:checked").val()==true ? true : false,
            OrderDisplay: $("#OrderDisplay").val()
        }
        var vld = window.baseJs.validation;
        var $form = $("#createoredit");
        var jsonData = JSON.stringify(model);
        $.ajax({
            url: '/admin/course/createoredit',
            data: jsonData,
            type: 'POST',
            contentType: 'application/json',
            dataType: 'json',
            cache: false,
            success: function (response) {
                if (response.result === true) {
                    $('#con-close-modal').modal('toggle');
                    window.location.reload();
                } else if (response.Errors) {
                    vld.clear($form);
                    vld.all(response.Errors, $form);
                } else {
                    $('#con-close-modal').modal('toggle');
                }
            }
        });
    },
};