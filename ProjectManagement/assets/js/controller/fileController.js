﻿var fileControler = {
    currentFile: "",
    deleteFile: function () {
        $.ajax({
            method: "POST",
            url: "/admin/file/delete",
            data: {
                filePath:fileControler.currentFile
            },
            async: false
        }).done(function (response) {
            window.location.reload();
        });
    },
};