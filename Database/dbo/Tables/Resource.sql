﻿CREATE TABLE [dbo].[Resource] (
    [Id]    INT            IDENTITY (1, 1) NOT NULL,
    [Name]  NVARCHAR (250) NOT NULL,
    [Value] NTEXT          NOT NULL,
    [Key]   VARCHAR (50)   NULL,
    CONSTRAINT [PK_Resource] PRIMARY KEY CLUSTERED ([Id] ASC)
);

