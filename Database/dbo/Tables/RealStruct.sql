﻿CREATE TABLE [dbo].[RealStruct] (
    [Id]          INT            IDENTITY (1, 1) NOT NULL,
    [Name]        NVARCHAR (250) NULL,
    [Content]     NTEXT          NULL,
    [Description] NVARCHAR (250) NULL,
    [Keyword]     NVARCHAR (250) NULL,
    CONSTRAINT [PK_RealStruct] PRIMARY KEY CLUSTERED ([Id] ASC)
);

