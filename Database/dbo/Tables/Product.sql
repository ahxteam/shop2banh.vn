﻿CREATE TABLE [dbo].[Product] (
    [Id]           INT             IDENTITY (1, 1) NOT NULL,
    [Name]         NVARCHAR (250)  NULL,
    [CategoryId]   INT             NOT NULL,
    [UrlCode]      NVARCHAR (250)  NULL,
    [Description]  NVARCHAR (250)  NULL,
    [Keyword]      NVARCHAR (250)  NULL,
    [Detail]       NTEXT           NULL,
    [ProductCode]  NVARCHAR (250)  NULL,
    [Original]     NVARCHAR (250)  NULL,
    [Subject]      NVARCHAR (250)  NULL,
    [Size]         NVARCHAR (250)  NULL,
    [Material]     NVARCHAR (250)  NULL,
    [Price]        DECIMAL (18, 4) NOT NULL,
    [View]         INT             NOT NULL,
    [Summary]      NTEXT           NULL,
    [Active]       BIT             NOT NULL,
    [DisplayOrder] INT             NOT NULL,
    [Thumb]        NVARCHAR (250)  NULL,
    CONSTRAINT [PK_Product] PRIMARY KEY CLUSTERED ([Id] ASC)
);

