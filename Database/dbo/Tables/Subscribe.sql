﻿CREATE TABLE [dbo].[Subscribe] (
    [Id]          INT            IDENTITY (1, 1) NOT NULL,
    [Name]        NVARCHAR (50)  NOT NULL,
    [Email]       NVARCHAR (50)  NOT NULL,
    [Phone]       NVARCHAR (50)  NOT NULL,
    [Content]     NTEXT          NOT NULL,
    [CreatedDate] DATETIME       NOT NULL,
    [Subject]     NVARCHAR (250) NOT NULL,
    CONSTRAINT [PK_Subscribe] PRIMARY KEY CLUSTERED ([Id] ASC)
);

