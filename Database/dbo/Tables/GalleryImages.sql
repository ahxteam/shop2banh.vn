﻿CREATE TABLE [dbo].[GalleryImages] (
    [Id]          INT            IDENTITY (1, 1) NOT NULL,
    [GalleryId]   INT            NOT NULL,
    [Image]       NVARCHAR (250) NOT NULL,
    [Description] NVARCHAR (250) NULL,
    CONSTRAINT [PK_GalleryImages] PRIMARY KEY CLUSTERED ([Id] ASC)
);

