﻿CREATE TABLE [dbo].[Category] (
    [Id]           INT            IDENTITY (1, 1) NOT NULL,
    [Name]         NVARCHAR (250) NOT NULL,
    [Active]       BIT            NOT NULL,
    [OrderDisplay] INT            NOT NULL,
    [Description]  NVARCHAR (250) NULL,
    [Parent]       INT            NOT NULL,
    [CreatedDate]  DATETIME       NOT NULL,
    [UserId]       VARCHAR (50)   NOT NULL,
    [UpdatedDate]  DATETIME       NOT NULL,
    [UpdatedUser]  VARCHAR (50)   NULL,
    [StatusId]     INT            NOT NULL,
    [UrlCode]      NVARCHAR (250) NOT NULL,
    [Thumb]        NVARCHAR (250) NULL,
    CONSTRAINT [PK_Category] PRIMARY KEY CLUSTERED ([Id] ASC)
);

