﻿CREATE TABLE [dbo].[RealStructImage] (
    [Id]           INT            IDENTITY (1, 1) NOT NULL,
    [RealStructId] INT            NULL,
    [Name]         NVARCHAR (250) NULL,
    [Image]        NVARCHAR (250) NULL,
    CONSTRAINT [PK_RealStructImage] PRIMARY KEY CLUSTERED ([Id] ASC)
);

