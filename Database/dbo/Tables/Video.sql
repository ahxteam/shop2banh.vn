﻿CREATE TABLE [dbo].[Video] (
    [Id]          INT             IDENTITY (1, 1) NOT NULL,
    [Name]        NVARCHAR (250)  NOT NULL,
    [CodeEmbeb]   NVARCHAR (250)  NOT NULL,
    [Content]     NTEXT           NULL,
    [CreatedDate] DATETIME        NOT NULL,
    [Thumb]       NVARCHAR (250)  NULL,
    [Description] NVARCHAR (250)  NULL,
    [Keyword]     NVARCHAR (250)  NULL,
    [UrlCode]     NVARCHAR (250)  NULL,
    [Active]      BIT             NOT NULL,
    [Summary]     NVARCHAR (1000) NOT NULL,
    CONSTRAINT [PK_Video] PRIMARY KEY CLUSTERED ([Id] ASC)
);

