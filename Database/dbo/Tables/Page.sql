﻿CREATE TABLE [dbo].[Page] (
    [Id]           INT            IDENTITY (1, 1) NOT NULL,
    [Name]         NVARCHAR (250) NOT NULL,
    [UrlCode]      VARCHAR (250)  NOT NULL,
    [Active]       BIT            NOT NULL,
    [OrderDisplay] INT            NOT NULL,
    [Description]  NVARCHAR (250) NOT NULL,
    [CreatedDate]  DATETIME       NOT NULL,
    [Content]      NTEXT          NULL,
    [Keyword]      NVARCHAR (250) NULL,
    CONSTRAINT [PK_Page] PRIMARY KEY CLUSTERED ([Id] ASC)
);

