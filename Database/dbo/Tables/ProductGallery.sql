﻿CREATE TABLE [dbo].[ProductGallery] (
    [Id]        INT            IDENTITY (1, 1) NOT NULL,
    [ProductId] INT            NOT NULL,
    [Thumb]     NVARCHAR (250) NULL,
    [Title]     NVARCHAR (250) NULL,
    CONSTRAINT [PK_ProductGallery] PRIMARY KEY CLUSTERED ([Id] ASC)
);

