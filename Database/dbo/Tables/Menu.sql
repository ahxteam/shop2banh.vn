﻿CREATE TABLE [dbo].[Menu] (
    [MenuId]       INT            IDENTITY (1, 1) NOT NULL,
    [Name]         NVARCHAR (250) NOT NULL,
    [UrlCode]      VARCHAR (250)  NOT NULL,
    [Active]       BIT            NOT NULL,
    [OrderDisplay] INT            NOT NULL,
    [Description]  NVARCHAR (250) NULL,
    [Parent]       INT            NOT NULL,
    [CreatedDate]  DATETIME       NULL,
    [UserId]       VARCHAR (50)   NULL,
    [UpdatedDate]  DATETIME       NULL,
    [UpdatedUser]  VARCHAR (50)   NULL,
    [StatusId]     INT            NULL,
    CONSTRAINT [PK_Menu] PRIMARY KEY CLUSTERED ([MenuId] ASC)
);

