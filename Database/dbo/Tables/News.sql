﻿CREATE TABLE [dbo].[News] (
    [NewsID]        INT             IDENTITY (1, 1) NOT NULL,
    [Title]         NVARCHAR (250)  NOT NULL,
    [UrlCode]       VARCHAR (250)   NOT NULL,
    [ThumbPic]      NVARCHAR (250)  NULL,
    [MenuID]        INT             NOT NULL,
    [Keyword]       NVARCHAR (500)  NOT NULL,
    [Description]   NVARCHAR (500)  NOT NULL,
    [View]          INT             NOT NULL,
    [Summary]       NVARCHAR (2000) NULL,
    [Content]       NTEXT           NOT NULL,
    [Active]        BIT             NOT NULL,
    [OrderDisplay]  INT             NOT NULL,
    [UserID]        VARCHAR (50)    NULL,
    [CreatedDate]   DATETIME        NULL,
    [UpdatedUserID] VARCHAR (50)    NULL,
    [UpdatedDate]   DATETIME        NULL,
    [StatusID]      INT             NULL,
    [Tags]          NVARCHAR (2500) NULL,
    CONSTRAINT [PK_News] PRIMARY KEY CLUSTERED ([NewsID] ASC)
);

