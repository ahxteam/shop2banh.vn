﻿CREATE TABLE [dbo].[Gallery] (
    [Id]          INT            IDENTITY (1, 1) NOT NULL,
    [Name]        NVARCHAR (250) NOT NULL,
    [Description] NVARCHAR (250) NULL,
    [Type]        INT            NOT NULL,
    [Active]      BIT            NOT NULL,
    CONSTRAINT [PK_Gallery] PRIMARY KEY CLUSTERED ([Id] ASC)
);

