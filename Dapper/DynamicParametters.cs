﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Security.Cryptography;
using System.Text;
using Enyim.Caching;

namespace Dapper
{
    public partial class DynamicParameters
    {
        /// <summary>
        /// Gets the list parameters.
        /// </summary>
        /// <value>
        /// The list parameters.
        /// </value>
        public Dictionary<string, string> ListParameters
        {
            get
            {
                var obj = new Dictionary<string, string>();
                if (parameters.Count > 0)
                {
                    foreach (var parameter in parameters)
                    {
                        obj.Add(parameter.Key, Convert.ToString(parameter.Value.Value));
                    }
                }
                return obj;
            }
        }

        /// <summary>
        /// Gets or sets the command current.
        /// </summary>
        /// <value>
        /// The command current.
        /// </value>
        public string CommandCurrent { get; set; }

        /// <summary>
        /// Gets or sets the key out params.
        /// </summary>
        /// <value>
        /// The key out params.
        /// </value>
        public string KeyOutParams { get; set; }

        /// <summary>
        /// Get the value of a parameter
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="name"></param>
        /// <returns>The value, note DBNull.Value is not returned, instead the value is returned as null</returns>
        public T GetDataOutput<T>(string name)
        {
            var val = parameters[Clean(name)].AttachedParam.Value;
            if (val == DBNull.Value)
            {
                //if (default(T) != null)
                //{
                //    throw new ApplicationException("Attempting to cast a DBNull to a non nullable type!");
                //}
                return default(T);
            }
            return (T)val;
        }

        /// <summary>
        /// Gets or sets the schema current.
        /// </summary>
        /// <value>
        /// The schema current.
        /// </value>
        public string SchemaCurrent { get; set; }

        /// <summary>
        /// Gets or sets the cache.
        /// </summary>
        /// <value>
        /// The cache.
        /// </value>
        public MemcachedClient Cache { get; set; }


        /// <summary>
        /// Gets a value indicating whether [use cache].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [use cache]; otherwise, <c>false</c>.
        /// </value>
        public bool UseCache
        {
            get
            {
                return ConfigurationManager.AppSettings["UseCache"].ToLower() == "true";
            }
        }

        /// <summary>
        /// Generates the key cache output.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        public string GenerateKeyCacheOutput(string key, string name)
        {
            return string.Format(ConfigurationManager.AppSettings["CacheFormatGenerateOutputParam"], key, name);
        }

        public static string Md5EnCrypt(string text)
        {
            MD5 md5 = new MD5CryptoServiceProvider();

            //compute hash from the bytes of text
            md5.ComputeHash(Encoding.ASCII.GetBytes(text));

            //get hash result after compute it
            var result = md5.Hash;

            var strBuilder = new StringBuilder();
            for (var i = 0; i < result.Length; i++)
            {
                //change it into 2 hexadecimal digits
                //for each byte
                strBuilder.Append(result[i].ToString("x2"));
            }

            return strBuilder.ToString();
        }
    }
}
